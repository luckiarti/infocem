
		</div>
		<!-- /.wrapper -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="footer__wrapp">
					<div class="footer__row row">
						<div class="footer__col">
							<a href="" class="footer__link footer__link_title">
								О выставке
							</a>
							<ul class="footer__list">
								<li class="footer__item">
									<a href="" class="footer__link">
										Организаторы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Деловая программа
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Статистика 2017
									</a>
								</li>

								<li class="footer__item">
									<a href="" class="footer__link">
										Отчёты по выставкам
									</a>
								</li>
							</ul>
						</div>
						<div class="footer__col">
							<a href="" class="footer__link footer__link_title">
								О конференции
							</a>
							<ul class="footer__list">
								<li class="footer__item">
									<a href="" class="footer__link">
										Забронировать стенд
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Гостиницы и визы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Список экспонентов
									</a>
								</li>

								<li class="footer__item">
									<a href="" class="footer__link">
										Руководство участников
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Рекламные возможности
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Официальная поддержка
									</a>
								</li>
							</ul>
						</div>
						<div class="footer__col">
							<a href="" class="footer__link footer__link_title">
								Посетителям
							</a>
							<ul class="footer__list">
								<li class="footer__item">
									<a href="" class="footer__link">
										Список участников
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Время работы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Гостиницы и визы
									</a>
								</li>
							</ul>

							<a href="" class="footer__link footer__link_title">
								Участникам
							</a>
							<ul class="footer__list">
								<li class="footer__item">
									<a href="" class="footer__link">
										Аккредитация прессы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Пресс-релизы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Баннеры и логотипы
									</a>
								</li>
							</ul>
						</div>
						<div class="footer__col">
							<a href="" class="footer__link footer__link_title">
								Прессе
							</a>
							<ul class="footer__list">
								<li class="footer__item">
									<a href="" class="footer__link">
										Аккредитация прессы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Пресс-релизы
									</a>
								</li>
								<li class="footer__item">
									<a href="" class="footer__link">
										Баннеры и логотипы
									</a>
								</li>
							</ul>
							<a href="" class="footer__link footer__link_title">
								Новости
							</a>
							<br>
							<a href="" class="footer__link footer__link_title">
								Контакты
							</a>
						</div>
						<div class="footer__lang">
							<a href="" class="lang">
								<span class="lang__text">en</span>
								<span class="lang__flag" style="background-image: url(<?php theme_uri()?>/images/flag-lang/en.jpg);"></span>
							</a>
						</div>
					</div>

					<ul class="social footer_social">
						<li class="social__item">
							<a href="" class="social__link">
								<?php include "images/social/social-vk.svg"; ?>
							</a>
						</li>
						<li class="social__item">
							<a href="" class="social__link">
								<?php include "images/social/social-fc.svg"; ?>
							</a>
						</li>
						<li class="social__item">
							<a href="" class="social__link">
								<?php include "images/social/social-tw.svg"; ?>
							</a>
						</li>
						<li class="social__item">
							<a href="" class="social__link">
								<?php include "images/social/social-in.svg"; ?>
							</a>
						</li>
						<li class="social__item">
							<a href="" class="social__link">
								<?php include "images/social/social-yt.svg"; ?>
							</a>
						</li>
					</ul>
					
				</div>
				<!-- /.footer__wrapp -->
			</div>
			<!-- /.container -->
		</footer>
		
		<!-- <script src="start.min.js"></script> -->
		<!-- <script src="end.min.js"></script> -->

		<!-- popup -->
		<?php include ('template_parts/pop_up/popup_book_stand.php'); ?>
		<?php include ('template_parts/pop_up/popup_free_ticket.php'); ?>
		<?php include ('template_parts/pop_up/popup_application_advertising.php'); ?>

		<?php wp_footer(); ?>
		<?php do_action('end'); ?>
	</body>
</html>