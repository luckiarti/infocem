<?php get_header(); ?>
			<header class="header" >
				<div class="container_fluid">
					<div class="header__data">
						<span>28-30 ноября 2018 г.</span>
						<span>Москва, Экспоцентр</span>
					</div>
					<h2 class="header__subtitle">
						<span>XX Международный <br> строительный форум</span>
						<span class="header__logotype" style="background-image: url(<?php theme_uri()?>/images/logotype/infcem2k18.png);"></span>
					</h2>
					<h1 class="header__title">
						Цемент <br> Бетон <br> Сухие смеси
					</h1>
					<div class="bunchBtn header__bunchBtn">
						<div class="bunchBtn__row row">
							<div class="bunchBtn__col">
								<a href="" class="btn btn_orange">
									<span>Выставка</span>
								</a>
							</div>
							<div class="bunchBtn__col">
								<a href="" class="btn btn_orange">
									<span>Конфереция</span>
								</a>
							</div>
							<div class="bunchBtn__col bunchBtn__col_fullwith">
								<a href="" class="btn btn_orangedarker">
									<span>Получить бесплатный билет</span>
								</a>
							</div>
						</div>
					</div>
					<div class="playBtn videoyt" data-video-id="ZjjprLkWpaA">
						<div class="playBtn__play" style="background-image: url(<?php theme_uri()?>/images/icon/icon-play.svg);"></div>
					</div>
				</div>	
				<!-- /.container_fluid -->
			</header>
			<!-- /.header -->
			<section class="abouTheExhibition">
				
				<div class="container">
					<h2 class="titleAvg">
						О выставке
					</h2>
					<div class="slider cube">
						<div class="slider__slider">
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt=""></div>
						</div>
					</div>
					<div class="abouTheExhibition__wrapp">
						<ul class="abouTheExhibition__list justi_cont">
							<li class="abouTheExhibition__item">
								<div class="abouTheExhibition__number">
									5 000
								</div>
								<p class="abouTheExhibition__text">
									делегатов из 18 стран
								</p>
							</li>
							<li class="abouTheExhibition__item">
								<div class="abouTheExhibition__number">
									5 000
								</div>
								<p class="abouTheExhibition__text">
									кв. м. выставочной площади
								</p>
							</li>
							<li class="abouTheExhibition__item">
								<div class="abouTheExhibition__number">
									150
								</div>
								<p class="abouTheExhibition__text">
									экспонатов
								</p>
							</li>
							
						</ul>
						<ul class="abouTheExhibition__list justi_cont">
							<li class="abouTheExhibition__item">
								<div class="abouTheExhibition__number">
									6 000
								</div>
								<p class="abouTheExhibition__text">
									посетителей
								</p>
							</li>
							<li class="abouTheExhibition__item">
								<div class="abouTheExhibition__number">
									80
								</div>
								<p class="abouTheExhibition__text">
									докладов
								</p>
							</li>
							<li class="abouTheExhibition__item">
								
							</li>
							
							
						</ul>
						
					</div> 
					<p class="abouTheExhibition__infotxt">
						Ежегодно на одной площадке собираются производители оборудования для производства цемента, бетона, железобетонных изделий, сухих строительных смесей, добавок и заполнителей, заводы ДСК, КПД. Выставка подкреплена одной из лучших деловых программ Европы, включающую выступления зарубежных экспертов строительной отрасли Европы, Азии и Ближнего Востока, кофе-брейки, обеды, круглый стол для детального обсуждения конкретных вопросов, а также интереснейшую культурно-развлекательную программу.
					</p>
					<div class="board">
						75% посетителей выставки — первые лица компании, принимающие решения.
					</div>
				</div>
			</section>
			<section class="sergininKonusu">
				<div class="container">
					<h2 class="titleAvg">
						Тематики выставки
					</h2>
					<div class="sergininKonusu__row row">
						<ul class="sergininKonusu__list justi_cont">
							<li class="sergininKonusu__item">
								Заводы «под ключ»
							</li>
							<li class="sergininKonusu__item">
								Тара, упаковка, транспортировка
							</li>
							<li class="sergininKonusu__item">
								Добавки в бетон и цемент, пигменты, заполнители
							</li>
							<li class="sergininKonusu__item">
								Лабораторное и аналитическое оборудование
							</li>
							<li class="sergininKonusu__item">
								Сырье и оборудование для его подготовки
							</li>
							<li class="sergininKonusu__item">
								Цемент, известь, гипс
							</li>
							<li class="sergininKonusu__item">
								Силоса, смесители, дозаторы
							</li>
							<li class="sergininKonusu__item">
								Системы управления и контроля качества продукции
							</li>
							<li class="sergininKonusu__item">
								Энергоэффективные технологии и автоматизация в строительстве
							</li>
							<li class="sergininKonusu__item">
								Арматура и опалубка
							</li>
						</ul>
					</div>
					<div class="board sergininKonusu__board">
						Идеальная площадка для установления взаимовыгодных контактов между поставщиками и потребителями
					</div>
					<div class="banner sergininKonusu__banner">
						<div class="banner__row row">
							<div class="banner__col banner__col_half">
								<div class="banner__block banner__block_220px">
									<div class="banner_advertising">
									</div>
								</div>
							</div>
							<div class="banner__col banner__col_half">
								<div class="banner__block banner__block_220px">
									<div class="banner_advertising">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="person sergininKonusu__person">
						<div class="person__personality">
							<div class="person__image">
								<img class="person__img" src="<?php theme_uri()?>/images/person/person-fulen.jpg" alt="">
							</div>
							<div class="person__data">
								<p class="person__name">
									Д-р Фу Лефень
								</p>
								<p class="person__status">
									Shanghai Sunrise Polymer Material, Китай
								</p>
							</div>
							
						</div>
						<div class="person__txt">
							Благодарю за поддержку и отличную организацию во время нашего визита в Москву. Нам очень понравилась как конференция, так и международная строительная выставка и другие организованные вами мероприятия. Мы познакомились с потенциальными партнерами и провели интересные технические и коммерческие переговоры.
						</div>
					</div>

					<div class="slider cube">
						<div class="slider__slider">
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt=""></div>
							<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt=""></div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- /.sergininKonusu -->
			<section class="organizerSupport">
				<div class="container">
					<h2 class="titleAvg">
						Тематики выставки
					</h2>
					<!-- /.titleAvg -->
					<div class="organizerSupport__wrapp">
						<div class="organizerSupport__organiz justi_cont">
							<div class="organizerSupport__orgtext">
								Организаторами Форума выступают <strong>журнал </strong> <span>
									<strong>
										«ALITinform: Цемент. Бетон. Сухие смеси»
									</strong>
									и <strong>Российский Союз строителей</strong>
								</span> 

							</div>
							<div class="organizerSupport__image">
								<img src="<?php theme_uri()?>/images/support/alitinform.png" alt="" class="organizerSupport__img">
								<img src="<?php theme_uri()?>/images/support/rus-union-builders.png" alt="" class="organizerSupport__img">
							</div>
						</div>
						<p class="organizerSupport__text">
							Форум проводится при официальной поддержке Государственной Думы, Правительства Москвы, Министерства строительства и жилищно-коммунального хозяйства РФ, Министерства промышленности и торговли РФ, Союза производителей цемента России, Российского союза производителей сухих строительных смесей, Евразийской Экономической комиссии, Международной Федерации по железобетону (FIB) и т.д.
						</p>
						<div class="organizerSupport__support justi_cont">
							<img src="<?php theme_uri()?>/images/support/state-duma.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/minstroy-russia.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/dark-dragon.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/white-dragon.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/rus-union-builders-grey.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/fib-ceb-fip.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/eak.png" alt="" class="organizerSupport__img">
							<img src="<?php theme_uri()?>/images/support/cpccc.png" alt="" class="organizerSupport__img">
						</div>
						<div class="person organizerSupport__person">
							<div class="person__personality">
								<div class="person__image">
									<img class="person__img" src="<?php theme_uri()?>/images/person/person-ralf-pasker.jpg" alt="">
								</div>
								<div class="person__data">
									<p class="person__name">
										Ральф Паскер
									</p>
									<p class="person__status">
										исполнительный директор, Европейская ассоциация СФТК, Германия
									</p>
								</div>
								
							</div>
							<div class="person__txt">
								Благодарю за приглашение принять участие в конференции. Мероприятие было прекрасно организовано. Для меня было большой честью выступить с докладом и председательствовать в рамках одной из сессий. Я не пожалел об участии в конференции. Во время мероприятия мне удалось установить ценные контакты.
							</div>
						</div>
						<div class="slider cube">
							<div class="slider__slider">
								<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/3.jpg" alt=""></div>
								<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/3.jpg" alt=""></div>
								<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/3.jpg" alt=""></div>
								<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/3.jpg" alt=""></div>
								<div class="slider__slide"><img src="<?php theme_uri()?>/images/foto-exhibition/3.jpg" alt=""></div>
							</div>
						</div>
					</div>
					<!-- /.organizerSupport__wrapp -->
				</div>
				<!-- /.container -->
			</section>
			<!-- /.organizerSupport -->
			<section class="programEvents">
				<div class="container">
					
					<div class="programEvents__wrapp justi_cont">
						<div class="programEvents__row row">
							<div class="programEvents__col programEvents__col_txt">
								<h2 class="titleAvg">
									Программа мероприятий
								</h2>
								<!-- /.titleAvg -->
								<ul class="programEvents__list">
									<li class="programEvents__item">
										<p class="programEvents__number" data-number="1">
											XIX Международная специализированная выставка
										</p>
										<p class="programEvents__text">
											«Цемент. Бетон. Сухие смеси»
										</p>
									</li>
									<li class="programEvents__item">
										<p class="programEvents__number" data-number="2">
											V Международная конференция
										</p>
										<p class="programEvents__text">
											«Индустриальное домостроение: производство, проектирование, строительство – BlockRead»
										</p>
									</li>
									<li class="programEvents__item">
										<p class="programEvents__number" data-number="3">
											XX Юбилейная международная конференция
										</p>
										<p class="programEvents__text">
											«Современные технологии сухих смесей в строительстве – MixBuild»
										</p>
									</li>

								</ul>
								<!-- /.programEvents__list -->
							</div>
							<div class="programEvents__col programEvents__col_banner">
								<div class="banner">
									<div class="banner__row row">
										<div class="banner__col banner__col_fullwith">
											<div class="banner__block banner__block_cube">
												<div class="banner_advertising">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="board programEvents__board">
							Деловая программа включает <span class="orange_color">50 уникальных докладов</span> от главных специалистов и ведущих аналитиков мирового и региональных рынков строительных материалов, представителей исследовательских центров, органов государственной власти и международных организаций.
						</div>
						<!-- /.board -->

						<ul class="programEvents__list">
							<li class="programEvents__item">
								<p class="programEvents__number" data-number="4">
									VI Международный семинар-конкурс молодых ученых и аспирантов, работающих в области вяжущих веществ, бетонов и сухих смесей
								</p>
							</li>
							<li class="programEvents__item">
								<p class="programEvents__number" data-number="5">
									Обучающий семинар для технологов компаний-производителей товарного бетона и ЖБИ
								</p>
							</li>
							<li class="programEvents__item">
								<p class="programEvents__number" data-number="6">
									Экскурсии на заводы по производству сухих строительных смесей и ведущие ДСК г. Москвы и Московской области
								</p>
							</li>

						</ul>
						<!-- /.programEvents__list -->
					</div>
					<!-- /.programEvents__wrapp -->
					
				</div>
				<!-- /.container -->
			</section>
			<!-- /.programEvents -->
			<section class="informationPartners">
				<div class="container">
					<h2 class="titleAvg">
						Информационные партнёры
					</h2>
					<div class="informationPartners__wrapp">
						<div class="informationPartners__row row">
							<ul class="informationPartners__list justi_cont">
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/1.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/2.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/3.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/4.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/5.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/6.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/1.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/1.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/2.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/3.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/4.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/5.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/6.png" alt="">
								</li>
								<li class="informationPartners__item">
									<img src="<?php theme_uri()?>/images/information-partners/1.png" alt="">
								</li>
							</ul>
						</div>
					</div>
					<!-- /.informationPartners__wrapp -->
				</div>
				<!-- /.container -->
			</section>
			<!-- /.informationPartners -->
			<section class="photoReport">
				<div class="container">
					<h2 class="titleAvg">
						Фотоотчет 2017 года
					</h2>
					<div class="photoReport__wrapp">
						<div class="photoReport__row row">
							<ul class="photoReport__list">
								<li class="photoReport__item photoReport__item_half">
									<div class="photoReport__image photoReport__image_half">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/1.jpg" alt="">
									</div>
									
								</li>
								<li class="photoReport__item photoReport__item_hole">
									<div class="photoReport__image photoReport__image_hole">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/2.jpg" alt="">
									</div>
								</li>
								<li class="photoReport__item photoReport__item_hole">
									<div class="photoReport__image photoReport__image_hole">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/3.jpg" alt="">
									</div>
								</li>
								<!-- 3 -->
								<li class="photoReport__item photoReport__item_hole">
									<div class="photoReport__image photoReport__image_hole">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/4.jpg" alt="">
									</div>
								</li>
								<li class="photoReport__item photoReport__item_half">
									<div class="photoReport__image photoReport__image_half">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/5.jpg" alt="">
									</div>
									
								</li>
								<li class="photoReport__item photoReport__item_hole">
									<div class="photoReport__image photoReport__image_hole">
										<img class="photoReport__img" src="<?php theme_uri()?>/images/photo-report/6.jpg" alt="">
									</div>
								</li>
							</ul>
							<!-- /.photoReport__list -->
						</div>
						<!-- /.row -->
						<div class="tac">
							<a href="" class="btn photoReport__btn">
								<span>Посмотреть полный отчёт</span>
							</a>
						</div>
						
					</div>
					<!-- /.photoReport__wrapp -->
				</div>
				<!-- /.container -->
			</section>
			<!-- /.photoReport -->
			<section class="reviews">
				<div class="container">
					<h2 class="titleAvg">
						Отзывы
					</h2>
					<!-- /.titleAvg -->
					<div class="reviews__wrapp">
						<div class="reviews__row row">
							<ul class="reviews__list justi_cont">
								<li class="reviews__item">
									<div class="person">
										<div class="person__personality">
											<div class="person__image">
												<img class="person__img" src="<?php theme_uri()?>/images/person/person-ralf-pasker.jpg" alt="">
											</div>
											<div class="person__data">
												<p class="person__name">
													Ральф Паскер
												</p>
												<p class="person__status">
													исполнительный директор, Европейская ассоциация СФТК, Германия
												</p>
											</div>
										</div>
									</div>
								</li>
								<li class="reviews__item">
									<div class="person">
										<div class="person__personality">
											<div class="person__image">
												<img class="person__img" src="<?php theme_uri()?>/images/person/person-ralf-pasker.jpg" alt="">
											</div>
											<div class="person__data">
												<p class="person__name">
													Ральф Паскер
												</p>
												<p class="person__status">
													исполнительный директор, Европейская ассоциация СФТК, Германия
												</p>
											</div>
										</div>
									</div>
								</li>
								<li class="reviews__item">
									<div class="person">
										<div class="person__personality">
											<div class="person__image">
												<img class="person__img" src="<?php theme_uri()?>/images/person/person-ralf-pasker.jpg" alt="">
											</div>
											<div class="person__data">
												<p class="person__name">
													Ральф Паскер
												</p>
												<p class="person__status">
													исполнительный директор, Европейская ассоциация СФТК, Германия
												</p>
											</div>
										</div>
									</div>
								</li>
								<li class="reviews__item">
									<div class="person">
										<div class="person__personality">
											<div class="person__image">
												<img class="person__img" src="<?php theme_uri()?>/images/person/person-ralf-pasker.jpg" alt="">
											</div>
											<div class="person__data">
												<p class="person__name">
													Ральф Паскер
												</p>
												<p class="person__status">
													исполнительный директор, Европейская ассоциация СФТК, Германия
												</p>
											</div>
										</div>
									</div>
								</li>
							</ul>
							<!-- /.reviews__list -->
						</div>
						<!-- /.row -->
						<h2 class="reviews__title">
							Ждём Вас на юбилейном XX Форуме ЦБСС 28–30 ноября 2018 года в Москве, ЦВК «Экспоцентр» Павильон №3.
						</h2>
						<div class="bunchBtn reviews__bunchBtn">
							<div class="bunchBtn__row row">
								<div class="bunchBtn__col">
									<a href="" class="btn btn_orange">
										<span>Выставка</span>
									</a>
								</div>
								<div class="bunchBtn__col">
									<a href="" class="btn btn_orange">
										<span>Конфереция</span>
									</a>
								</div>
								<div class="bunchBtn__col bunchBtn__col_fullwith">
									<a href="" class="btn btn_orangedarker">
										<span>Получить бесплатный билет</span>
									</a>
								</div>
							</div>
						</div>

						<div class="reviews__image">
							<img class="reviews__img" src="<?php theme_uri()?>/images/reviews__people.jpg" alt="">
						</div>
					</div>
					<!-- /.reviews__wrapp -->
				</div>
				<!-- /.container -->
			</section>
			<!-- /.reviews -->

<?php get_footer(); ?>