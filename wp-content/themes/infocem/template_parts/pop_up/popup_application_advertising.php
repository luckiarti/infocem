<div class="modal-open" data-modal-target="modal_3" style="display: block; background-color: red; padding: 20px;">open Three</div>


<div class="modal-popup" data-modal-id="modal_3">
	<div class="modal-popup-block popup_half">
		<div class="modal-close popup__close"></div>
		<div class="popup">
			<div class="popup__container">
				<h2 class="popup__title">
			 		Заявка на рекламу
			 	</h2>
			 	<p class="popup__text">
			 		Оставьте свои контакты и наш менеджер свяжется с вами и ответит на все ваши вопросы
			 	</p>
			 	<form action="" class="popup__form">
			 		<div class="popup__row row">
			 			<div class="popup__col full">
							<input type="text" placeholder="Ваше имя" class="field__input">
						</div>
						<div class="popup__col full">
							<input type="text" placeholder="Ваш телефон" class="field__input">
						</div>
						<div class="popup__col full">
							<input type="text" placeholder="Ваш e-mail" class="field__input">
						</div>
						<div class="popup__col full">
							<button class="btn">
								<span>
									Оставить заявку
								</span>
							</button>
						</div>
						<div class="popup__col full">
							<p class="field__agreement popup__agreement">
								Нажимая кнопку «Оставить заявку», вы соглашаетесь с <a href=""> политикой конфиденциальности </a> 
							</p>
						</div>
			 		</div>
			 	
					
				</form>
			</div>
		</div>
	</div>
</div>

