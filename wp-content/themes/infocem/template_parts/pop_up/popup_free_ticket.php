<div class="modal-open" data-modal-target="modal_2" style="display: block; background-color: red; padding: 20px;">open TWO</div>


<div class="modal-popup" data-modal-id="modal_2">
	<div class="modal-popup-block">
		<div class="modal-close popup__close"></div>
		<div class="popup">
			<div class="popup__container">
				<h2 class="popup__title">
			 		Получить бесплатный билет
			 	</h2>
			 	<p class="popup__text">
			 		Вы можете получить электронный билет на Международную специализированную выставку «Цемент. Бетон. Cухие смеси-2018» совершенно бесплатно.  Для этого заполните, пожалуйста, регистрационную форму ниже
			 	</p>
			 	<ul class="popup__listtxt">
			 		<li class="popup__itemtxt">
			 			1.	До 28 ноября 2018 года заполните регистрационную форму и получите электронный билет на указанный Вами e-mail
			 		</li>
			 		<li class="popup__itemtxt">
			 			2.	Распечатайте билет и возьмите с собой на выставку
			 		</li>
			 		<li class="popup__itemtxt">
			 			3.	Обменяйте билет на бейдж посетителя на одной из стоек регистрации при входе в ЦВК «Экспоцентр»
			 		</li>
			 		<li class="popup__itemtxt">
			 			4. Не забудьте получить подарок за онлайн-регистрацию при получении бейджа
			 		</li>
			 	</ul>
			 	<form action="" class="popup__form">
			 		<h3 class="popup__subtitle">
			 			Контактное лицо
			 		</h3>
			 		<div class="popup__row row">
			 			<div class="popup__col">
							<input type="text" placeholder="Фамилия" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Должность" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Имя" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Контактный телефон" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Отчество" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="E-mail" class="field__input">
						</div>
			 		</div>
			 		<!-- /.popup__row row -->
			 		<h3 class="popup__subtitle">
			 			О компании
			 		</h3>
			 		<div class="popup__row row">
			 			<div class="popup__col">
							<input type="text" placeholder="Название компании с ОПФ" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Адрес" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Страна" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Индекс" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Город" class="field__input">
						</div>
						<div class="popup__col">
							<input type="text" placeholder="Сайт" class="field__input">
						</div>
			 		</div>
			 		<!-- /.popup__row row -->
			 		<h3 class="popup__subtitle">
			 			Сфера деятельности
			 		</h3>
			 		<div class="popup__row row">
			 			<div class="popup__col">
							<div class="selectClick">
								<span class="select__selected">Производство</span>
								<ul class="select__list">
									<li>Ку1</li>
									<li>Ку2 Ку2Ку2vКу2Ку2Ку2Ку2Ку 2Ку2Ку2 Ку2Ку2К у2Ку 2Ку2Ку2Ку2К у2Ку2Ку2Ку2Ку2Ку2Ку2</li>
									<li>Ку3</li>
									<li>Кawdawу3</li>
									<li>Кd awd у3</li>
									<li>Ку3</li>
									<li>Кaw d awу3</li>
									<li>К daw у3</li>
									<li>Куd aw 3</li>
									<li>К awd waу4</li>
									<li>К  awd у5</li>
								</ul>
							</div>
						</div>
						<div class="popup__col">
							<div class="selectClick">
								<span class="select__selected">Поставки</span>
								<ul class="select__list">
									<li>Ку1</li>
									<li>Ку2 Ку2Ку2vКу2Ку2Ку2Ку2Ку 2Ку2Ку2 Ку2Ку2К у2Ку 2Ку2Ку2Ку2К у2Ку2Ку2Ку2Ку2Ку2Ку2</li>
									<li>Ку3</li>
									<li>Кawdawу3</li>
									<li>Кd awd у3</li>
									<li>Ку3</li>
									<li>Кaw d awу3</li>
									<li>К daw у3</li>
									<li>Куd aw 3</li>
									<li>К awd waу4</li>
									<li>К  awd у5</li>
								</ul>
							</div>
						</div>
			 			<div class="popup__col">
							<input type="text" placeholder="Другое" class="field__input">
						</div>
			 		</div>
			 		<!-- /.popup__row row -->
			 		<h3 class="popup__subtitle">
			 			Прочая информация
			 		</h3>
			 		<div class="popup__row row">
			 			<div class="popup__col">
							<div class="selectClick">
								<span class="select__selected">Как вы узнали о выставке?</span>
								<ul class="select__list">
									<li>Ку1</li>
									<li>Ку2 Ку2Ку2vКу2Ку2Ку2Ку2Ку 2Ку2Ку2 Ку2Ку2К у2Ку 2Ку2Ку2Ку2К у2Ку2Ку2Ку2Ку2Ку2Ку2</li>
									<li>Ку3</li>
									<li>Кawdawу3</li>
									<li>Кd awd у3</li>
									<li>Ку3</li>
									<li>Кaw d awу3</li>
									<li>К daw у3</li>
									<li>Куd aw 3</li>
									<li>К awd waу4</li>
									<li>К  awd у5</li>
								</ul>
							</div>
						</div>
						<div class="popup__col">
							<div class="selectClick">
								<span class="select__selected">Цель посещения</span>
								<ul class="select__list">
									<li>Ку1</li>
									<li>Ку2 Ку2Ку2vКу2Ку2Ку2Ку2Ку 2Ку2Ку2 Ку2Ку2К у2Ку 2Ку2Ку2Ку2К у2Ку2Ку2Ку2Ку2Ку2Ку2</li>
									<li>Ку3</li>
									<li>Кawdawу3</li>
									<li>Кd awd у3</li>
									<li>Ку3</li>
									<li>Кaw d awу3</li>
									<li>К daw у3</li>
									<li>Куd aw 3</li>
									<li>К awd waу4</li>
									<li>К  awd у5</li>
								</ul>
							</div>
						</div>
			 			<div class="popup__col">
							<div class="selectClick">
								<span class="select__selected">Что вас интересует?</span>
								<ul class="select__list">
									<li>Ку1</li>
									<li>Ку2 Ку2Ку2vКу2Ку2Ку2Ку2Ку 2Ку2Ку2 Ку2Ку2К у2Ку 2Ку2Ку2Ку2К у2Ку2Ку2Ку2Ку2Ку2Ку2</li>
									<li>Ку3</li>
									<li>Кawdawу3</li>
									<li>Кd awd у3</li>
									<li>Ку3</li>
									<li>Кaw d awу3</li>
									<li>К daw у3</li>
									<li>Куd aw 3</li>
									<li>К awd waу4</li>
									<li>К  awd у5</li>
								</ul>
							</div>
						</div>
			 		</div>
			 		<!-- /.popup__row row -->


					<div class="popup__row row">
						<div class="popup__col full">
							<div class="popup__ask justi_cont">
								<div class="popup__asktext">
									Посещали ли вы прошлую выставку «Цемент. Бетон. Сухие смеси»?
								</div>
								<div class="popup__askckeck">
									<div class="popup__row row">
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_1" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">да</span>
												</span>
											</label>
										</div>
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_1" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">нет</span>
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="popup__col full ">
							<div class="popup__ask justi_cont">
								<div class="popup__asktext">
									Хотели бы Вы принять участие в мероприятиях деловой программы выставки?
								</div>
								<div class="popup__askckeck">
									<div class="popup__row row">
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_2" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">да</span>
												</span>
											</label>
										</div>
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_2" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">нет</span>
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="popup__col full ">
							<div class="popup__ask justi_cont">
								<div class="popup__asktext">
									Хотели бы Вы получать информацию о мероприятиях по тематике цемент, бетон, сухие смеси?
								</div>
								<div class="popup__askckeck">
									<div class="popup__row row">
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_3" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">да</span>
												</span>
											</label>
										</div>
										<div class="popup__col">
											<label class="field__label">
												<input class="field__radio" name="yes_3" type="radio">
												<span class="field__yesno">
													<span class="field__dots"></span>
													<span class="field__text">нет</span>
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="popup__col full">
							<textarea class="field__textarea" placeholder="Ваш комментарий (необязательно)"></textarea>
						</div>
					</div>
					<div class="popup__row row">
						<div class="popup__col">
							<button class="btn">
								<span>
									Подать заявку
								</span>
							</button>
						</div>
						<div class="popup__col">
							<p class="field__agreement popup__agreement">
								Нажимая кнопку «Подать заявку», вы соглашаетесь с <a href=""> политикой конфиденциальности </a> 
							</p>
						</div>
					</div>
					<!-- /.row -->
				</form>
			</div>
		</div>
	</div>
</div>

