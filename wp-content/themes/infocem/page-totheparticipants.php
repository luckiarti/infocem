<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: to the participants
 */
get_header(); ?>

	<header class="headerParticipants">
		<div class="container_fluid">
			<div class="headerParticipants__wrapp">
				<div class="headerParticipants__row row">
					<div class="headerParticipants__col headerParticipants__col_left">
						<h2 class="titleAvg">
							Участникам выставки
						</h2>
						<p class="abouTheExhibition__infotxt">
							Ежегодно на одной площадке собираются производители оборудования для производства цемента, бетона, железобетонных изделий, сухих строительных смесей, добавок и заполнителей, заводы ДСК, КПД.   
						</p>
						<p class="abouTheExhibition__infotxt">
							Выставка подкреплена одной из лучших деловых программ Европы, включающую выступления зарубежных экспертов строительной отрасли Европы, Азии и Ближнего Востока, кофе-брейки, обеды, круглый стол для детального обсуждения конкретных вопросов, а также интереснейшую культурно-развлекательную программу.
						</p>
						<p class="abouTheExhibition__infotxt last_margin">
							На этой выставке Вас ждут:
						</p>
					</div>
					<!-- /.headerParticipants__col -->
					<div class="headerParticipants__col headerParticipants__col_right">
						<div class="headerParticipants__image cube">
							<img class="headerParticipants__img" src="<?php theme_uri()?>/images/foto-exhibition/1.jpg" alt="">
						</div>
					</div>
					<!-- /.headerParticipants__col -->
				</div>
				<!-- /.headerParticipants__row row -->
				<div class="headerParticipants__row row">
					<ul class="headerParticipants__list justi_cont">
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Новые контакты
								</h3>
								<p class="blockSignatureLine__text">
									«Цемент. Бетон. Сухие смеси» — идеальная площадка для установления контактов между поставщиками и потребителями, проведения переговоров и заключения контрактов
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Возможности для бизнеса
								</h3>
								<p class="blockSignatureLine__text">
									Вы будете иметь возможность показать свою продукцию огромному количеству клиентов, специалистов и экспертов отрасли
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Продвижение продукции
								</h3>
								<p class="blockSignatureLine__text">
									Возможность продвижения своего продукта во всех регионах России
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Посетители со всей России
								</h3>
								<p class="blockSignatureLine__text">
									7500 посетителей со всех регионов России за три дня работы выставки
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Директора и заместители
								</h3>
								<p class="blockSignatureLine__text">
									75% посетителей выставки — первые лица компаний, принимающие решения
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Производители стройматериалов
								</h3>
								<p class="blockSignatureLine__text">
									52% посетителей — производители строительных материалов, в основном сухих строительных смесей, бетона и цемента
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
					</ul>
					<!-- /.headerParticipants__list -->
					<div class="tac ">
						<a href="" class="btn headerParticipants__btn">
							<span>Забронировать стенд</span>
						</a>
					</div>
				</div>
				<!-- /.headerParticipants__row row -->
			</div>
			<!-- /.headerParticipants__wrapp -->
		</div>	
		<!-- /.container_fluid -->
	</header>
	<!-- /.headerConference -->
	<section class="listparticipants">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Список участников
			</h2>
			<div class="listparticipants__slider">
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
			</div>
			<!-- /.listparticipants__slider -->
			<div class="listparticipants__panelslide board">
				<div class="listparticipants__slideleft"><span>ПРЕД</span></div>
				<p class="listparticipants__num">
					<span class="count">1</span>/
					<span class="quantity"></span>
				</p>
				<div class="listparticipants__slideright"><span>СЛЕД</span></div>
			</div>
			
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.listparticipants -->
	<section class="advertising">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Рекламные возможности для Вашего бизнеса
			</h2>
			<p class="abouTheExhibition__infotxt advertising__text">
				Предлагаем Вашему вниманию дополнительные рекламные возможности на конференции, которые помогут Вашей компании получить комплексное эффективное продвижение в рамках мероприятия.
			</p>
			<div class="advertising__row row">

				<div class="advertising__col">
					<div class="advertising__box">
						<h3 class="advertising__subtitle">
							Реклама в Каталоге выставки или журнале «ALITinform»
						</h3>
						<ul class="advertising__list">
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
						</ul>
					</div>
					<div class="advertising__box">
						<h3 class="advertising__subtitle">
							Реклама в Каталоге выставки или журнале «ALITinform»
						</h3>
						<ul class="advertising__list">
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200 на официальном сайте <a href="">http://infocem.info/</a>  на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200 на официальном сайте <a href="">http://infocem.info/</a>  на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							
						</ul>
						<p class="advertising__maket">
							*Макеты предоставляются экспонентом
						</p>
					</div>
				</div>
				<div class="advertising__col">
					<div class="advertising__box">
						<h3 class="advertising__subtitle">
							Реклама в Путеводителе выставки (тираж 6000 экз.)
						</h3>
						<ul class="advertising__list">
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									1-ая полоса (обложка) 1-ая полоса (обложка) 1-ая полоса (обложка)
								</span>
								<span class="advertising__price">
									112 000 ₽
								</span>
							</li>
							
						</ul>
					</div>
					<div class="advertising__box">
						<h3 class="advertising__subtitle">
							Дополнительные рекламные возможности
						</h3>
						<ul class="advertising__list">
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200  на баннера 200 х 200  на официальном сайте на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200  на баннера 200 х 200  на официальном сайте на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200  на баннера 200 х 200  на официальном сайте на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							<li class="advertising__item justi_cont">
								<span class="advertising__name">
									Размещение сквозного баннера 200 х 200  на баннера 200 х 200  на официальном сайте на 3 месяца*
								</span>
								<span class="advertising__price">
									33 000 ₽
								</span>
							</li>
							

							
						</ul>
						<p class="advertising__maket">
							*Макеты предоставляются экспонентом
						</p>
						<p class="advertising__maket">
							**Экспонентам предоставляется скидка 10%
						</p>
					</div>
				</div>
			</div>
			<!-- /.advertising__row row -->
			<div class="tac ">
				<a href="" class="btn advertising__btn">
					<span>Оставить заявку на рекламу</span>
				</a>
			</div>
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->
	<section class="advertising workingHours ">
		<div class="container_fluid">
			<div class="advertising__row row">

				<div class="advertising__col">
					<div class="cube_right_bottom">
						<div class="advertising__box workingHours__box workingHours__box_worktime">
							<h2 class="titleAvg">
								Время работы выставки
							</h2>
							<ul class="advertising__list workingHours__list">
								<li class="advertising__item workingHours__item_head">
									<span class="workingHours__data">
										дата
									</span>
									<span class="workingHours__time">
										Время
									</span>
									<span class="workingHours__event">
										Событие
									</span>
								</li>
								<li class="advertising__item advertising__item_body">
									<span class="workingHours__data">
										26 ноября
									</span>
									<span class="workingHours__time">
										08:00 - 20:00
									</span>
									<span class="workingHours__event">
										Монтаж стендов  (необорудованная площадь)
									</span>
								</li>
								<li class="advertising__item advertising__item_body">
									<span class="workingHours__data">
										26 ноября
									</span>
									<span class="workingHours__time">
										08:00 - 20:00
									</span>
									<span class="workingHours__event">
										Монтаж стендов  (необорудованная площадь)
									</span>
									<div class="advertising__item_multi">
										<span class="workingHours__time">
											08:00 - 20:00
										</span>
										<span class="workingHours__event">
											Монтаж стендов  (необорудованная площадь)
										</span>
									</div>
								</li>
								<li class="advertising__item advertising__item_body">
									<span class="workingHours__data">
										26 ноября
									</span>
									<span class="workingHours__time">
										08:00 - 20:00
									</span>
									<span class="workingHours__event">
										Монтаж стендов  (необорудованная площадь)
									</span>
									<div class="advertising__item_multi">
										<span class="workingHours__time">
											08:00 - 20:00
										</span>
										<span class="workingHours__event">
											Монтаж стендов  (необорудованная площадь)
										</span>
									</div>
								</li>
								<li class="advertising__item advertising__item_body">
									<span class="workingHours__data">
										26 ноября
									</span>
									<span class="workingHours__time">
										08:00 - 20:00
									</span>
									<span class="workingHours__event">
										Монтаж стендов  (необорудованная площадь)
									</span>
									<div class="advertising__item_multi">
										<span class="workingHours__time">
											08:00 - 20:00
										</span>
										<span class="workingHours__event">
											Монтаж стендов  (необорудованная площадь)
										</span>
									</div>
									<div class="advertising__item_multi">
										<span class="workingHours__time">
											08:00 - 20:00
										</span>
										<span class="workingHours__event">
											Монтаж стендов  (необорудованная площадь)
										</span>
									</div>
								</li>
								
							</ul>
						</div>
					</div>
					
					
				</div>
				<div class="advertising__col">
					<div class="cube_left_top">
						<div class="advertising__box workingHours__box workingHours__box_leadership">
							<h2 class="titleAvg">
								Руководство участника
							</h2>
							<p class="abouTheExhibition__infotxt workingHours__text">
								Организационный комитет рад приветствовать Вас на центральном событии отрасли!
							</p>
							<p class="abouTheExhibition__infotxt workingHours__text">
								Данное руководство содержит важную информацию, которая поможет сделать Ваше участие в выставке наиболее эффективным.
								Убедительно просим внимательно ознакомиться  с руководством и своевременно заполнять и отправлять организаторам соответствующие формы!
							</p>
							<a href="" class="workingHours__download">
								Скачать руководство экспонента выставки
							</a>
						</div>
					</div>
					<div class="advertising__row row">
						<div class="advertising__col">
							<div class="banner__block banner__block_cube_fullhalf">
								<div class="banner_advertising"></div>
							</div>
						</div>
						<div class="advertising__col">
							<div class="banner__block banner__block_cube_fullhalf">
								<div class="banner_advertising"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.workingHours -->
	<section class="advertising invitingCustomers">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Приглашение клиентов на стенд
			</h2>
			<p class="abouTheExhibition__infotxt advertising__text">
				Вы можете увеличить количество посетителей на стенде компании и повысить эффективность вашего участия в выставке, используя уже подготовленные нами рекламные материалы.
			</p>
			<div class="advertising__row row">

				<div class="advertising__col">
					<div class="advertising__box invitingCustomers__box invitingCustomers__box_big">
						<h2 class="invitingCustomers__title">
							Рассылка билетов вашим партнерам
						</h2>
						<p class="abouTheExhibition__infotxt workingHours__text">
							Направьте приглашение на стенд вашей компании своим клиентам  и партнерам, предоставив им возможность ознакомиться с вашей продукцией на выставке.
						</p>
						<a href="" class="workingHours__download">
							Скачать шаблон письма — приглашения
						</a>
						
					</div>
					<div class="advertising__box invitingCustomers__box invitingCustomers__box_small">
						<h2 class="invitingCustomers__title">
							Рассылка билетов вашим партнерам
						</h2>
						<p class="abouTheExhibition__infotxt workingHours__text">
							Направьте приглашение на стенд вашей компании своим клиентам  и партнерам, предоставив им возможность ознакомиться с вашей продукцией на выставке.
						</p>
						<a href="" class="btn invitingCustomers__btn">
							<span>Заказать приглашения</span>
						</a>
					</div>
				</div>
				<div class="advertising__col">
					<div class="advertising__box invitingCustomers__box invitingCustomers__box_big">
						<h2 class="invitingCustomers__title">
							Размещение баннера и новости на сайте вашей компании
						</h2>
						<p class="abouTheExhibition__infotxt workingHours__text">
							Разместите баннер и новость об участии в Международной строительной выставке “Цемент.Бетон.Сухие смеси” на сайте вашей компании с указанием места расположения стенда и категории представленной продукции. Посетители вашего сайта смогут вживую ознакомиться с продукцией и обсудить возможности сотрудничества на выставке.
						</p>
						<a href="" class="workingHours__download">
							Скачать шаблон новости
						</a>
						<br>
						<a href="" class="workingHours__download">
							Скачать баннеры и логотипы
						</a>
						
					</div>
					<div class="advertising__box invitingCustomers__box invitingCustomers__box_small">
						<h2 class="invitingCustomers__title">
							Размещение новости вашей компании на сайте выставки
						</h2>
						<p class="abouTheExhibition__infotxt workingHours__text">
							Вы можете присылать новости вашей компании для размещения на сайте нашей выставки, что обеспечит дополнительный интерес целевой аудитории к вашей продукции еще до начала Международной строительной выставки «Цемент.Бетон.Сухие смеси»
						</p>
						<a href="" class="btn invitingCustomers__btn">
							<span>Отправить новость</span>
						</a>
					</div>
				</div>
				
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->

	<section class="advertising hotelBooking">
		<div class="container_fluid">
			<div class="advertising__row row">

				<div class="advertising__col">
					<h2 class="titleAvg">
						Бронирование гостиниц для посетителей выставки
					</h2>
					<p class="abouTheExhibition__infotxt advertising__text hotelBooking__text">
						Вы можете увеличить количество посетителей на стенде компании и повысить эффективность вашего участия в выставке, используя уже подготовленные нами рекламные материалы.
					</p>
					<p class="abouTheExhibition__infotxt advertising__text hotelBooking__text hotelBooking__text_small">
						Цены указаны с учетом завтрака и трансфера до места проведения выставки и конференции. НДС  не облагаются. За ранний заезд (до 14.00) и поздний выезд (после 12.00) взимается дополнительная плата за проживание. Пожалуйста, указывайте время заезда и выезда. Предлагаем услуги по встрече  и проводам в аэропорту и на ж/д вокзалах: Аэропорт (ж/д вокзал) – Гостиница // Гостиница — Аэропорт (ж/д вокзал) — 3 300 рублей.
					</p>
				</div>
				<div class="advertising__col">
					<div class="banner hotelBooking__banner">
						<div class="banner__block banner__block_280px">
							<div class="banner_advertising">
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.advertising__row row -->
			<div class="advertising__row row">
				<div class="advertising__col">
					<div class="advertising__box justi_cont">
						<div class="justi_cont">
							<div class="hotelBooking__pricenum">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Стоимость номеров
								</h3>
								<ul class="advertising__list">
									<li class="advertising__item hotelBooking__item_head justi_cont">
										<span class="hotelBooking__type">Тип размещения</span>
										<span class="hotelBooking__price">Цена/сутки</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
								</ul>
							</div>
							<!-- /.hotelBooking__pricenum -->
							<div class="hotelBooking__gallery">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Галерея отеля
								</h3>
								<ul class="gallery">
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/1.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/1.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/2.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/2.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/3.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/3.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/4.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/4.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/5.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/6.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/7.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/7.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/8.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/8.jpg" alt="">
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						<div class="tac">
							<a href="" class="btn invitingCustomers__btn hotelBooking__btn">
								<span>Получить бесплатный билет</span>
							</a>
						</div>
						
						
					</div>
				</div>
				<div class="advertising__col">
					<div class="advertising__box justi_cont">
						<div class="justi_cont">
							<div class="hotelBooking__pricenum">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Стоимость номеров
								</h3>
								<ul class="advertising__list">
									<li class="advertising__item hotelBooking__item_head justi_cont">
										<span class="hotelBooking__type">Тип размещения</span>
										<span class="hotelBooking__price">Цена/сутки</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
								</ul>
							</div>
							<!-- /.hotelBooking__pricenum -->
							<div class="hotelBooking__gallery">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Галерея отеля
								</h3>
								<ul class="gallery">
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/1.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/1.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/2.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/2.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/3.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/3.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/4.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/4.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/5.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/6.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/7.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/7.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/8.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/8.jpg" alt="">
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						<div class="tac">
							<a href="" class="btn invitingCustomers__btn hotelBooking__btn">
								<span>Получить бесплатный билет</span>
							</a>
						</div>
						
						
					</div>
				</div>
				
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->

	<section class="advertising visaSvisitors">
		<div class="container_fluid">
			<div class="advertising__row row">
				<div class="advertising__col">
					<h2 class="titleAvg">
						Визы для посетителей выставки
					</h2>
					<p class="abouTheExhibition__infotxt advertising__text visaSvisitors__text">
						Все иностранные граждане должны получить визу для въезда на территорию Российской Федерации. Условия для получения российской визы могут меняться в зависимости от страны проживания иностранного гражданина, поэтому свяжитесь с посольством Российской Федерации в вашей стране, чтобы получить точную информацию о необходимых документах.
					</p>
					<p class="abouTheExhibition__infotxt advertising__text visaSvisitors__text">
						Наша компания может предоставить иностранным участникам выставок и конференций визовую поддержку для получения деловой или туристической визы в Российскую Федерацию.
					</p>
				</div>
				<div class="advertising__col">
					<div class="visaSvisitors__box">
						<div class="visaSvisitors__image cube_left_bottom">
							<img class="visaSvisitors__img" src="<?php theme_uri()?>/images/passport.jpg" alt="">
						</div>
					</div>
					
				</div>
			</div>
			<!-- /.advertising__row row -->
			<div class="advertising__row visaSvisitors__visainfo row">
				<div class="advertising__col">
					<h2 class="titleAvg visaSvisitors__titleAvg orange_color">
						Деловая виза
					</h2>
					<div class="visaSvisitors__desc">
						<h3 class="visaSvisitors__subtitle">
							Для Граждан других стран
						</h3>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для получения деловой Российской визы требуется официальное приглашение Федеральной Миграционной Службы или Министерства Иностранных Дел РФ. 
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
						Стоимость приглашения составляет <strong>25 евро.</strong> 
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Подготовить Официальное приглашение займет <strong>от 14 до 18 дней</strong>, поэтому рекомендуем Вам подготовить и отправить все необходимые документы <strong>
								до 20 октября 2018 года. 
							</strong>
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Пакет документов включает Заявку на Официальное приглашение  и копию паспорта с фотографией. В зависимости от страны консульство может требовать копию или оригинал приглашения. Свяжитесь с вашим консульством для получения более подробной информации. В случае необходимости вы можете заказать курьерскую доставку через нашу компанию или самостоятельно.

						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
						Для оформления Официального приглашения гражданам других стран, Вам необходимо:
						</p>
						<ul class="participantsMixBild__list visaSvisitors__list">
							<li class="participantsMixBild__item">
								Прислать копию страницы паспорта с фотографией
							</li>
							<li class="participantsMixBild__item">
								Заполнить Заявку на официальное приглашение
							</li>
						</ul>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для некоторых стран (например, Китай, Индия и др.) доступен только этот вид приглашения. Для получения более подробной информации, пожалуйста, <a href="">свяжитесь с нами</a>.
						</p>
					</div>
					
				</div>
				<div class="advertising__col">
					<h2 class="titleAvg visaSvisitors__titleAvg transparent">
						txt
					</h2>
					<div class="visaSvisitors__desc">
						<h3 class="visaSvisitors__subtitle">
							Для граждан Ес и норвегии
						</h3>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для граждан ЕС и Норвегии (кроме Великобритании и Ирландии)  мы бесплатно предоставляем копию приглашения для получения деловой визы. В большинстве случаев для получения визы гражданами вышеупомянутых стран достаточно копии приглашения. Однако консульство оставляет за собой право затребовать оригинал приглашения. В этом случае вы можете заказать курьерскую доставку через нашу компанию или самостоятельно.
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для оформления Официального приглашения Вам необходимо прислать <strong>копию страницы паспорта с фотографией</strong>.
						</p>
						
						
						
					</div>
					
				</div>
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->
	<section class="mixBildBlock participate bookTicket" style="background-image: url(<?php theme_uri()?>/images/bg/bg-participate.jpg);">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="participate__title">
								Забронировать стенд
							</h2>
							<p class="participate__text">
								Такое событие определенно стоит посетить. Мы ждём Вас на юбилейном XX <br> Форуме ЦБСС 28–30 ноября 2018 года в Москве, ЦВК «Экспоцентр» Павильон №3.
							</p>
							<a href="" class="btn btn_w260">
								<span>
									Оформить бронирование
								</span>
							</a>
						</div>
					</div>
				
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock -->


<?php get_footer() ?>