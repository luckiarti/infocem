<?php get_header(); ?>



	<?php if( have_posts() ){ while( have_posts() ){ the_post(); ?>

		<section class="post" <?php post_class(); ?> id="post-<?php the_ID(); ?>">
			

			<!-- post -->
			
			<div class="container_fluid">
				<div class="exhibition__row row">

					<div class="exhibition__col">
						<p class="blockSignatureLine__data">
							<?php the_time('d.m.Y'); ?>				
						</p>
						<h3 class="exhibition__title"><?php the_title(); ?></h3>
						<?php the_content(); ?>
						
						<div class="post__desc">
							<?php echo get_post_meta( $post->ID, 'paste_list', true ); ?>
						</div>
						<?php $image = get_post_meta( $post->ID, 'paste_fotoNews', true ); ?>
						
						<div class="post__image">
							<img src="<?php echo wp_get_attachment_url($image); ?>" alt="">
						</div>
						

						<?php echo get_post_meta( $post->ID, 'paste_text_end', true ); ?>
					</div>
					<div class="exhibition__col exhibition__col_order">
						<div class="exhibition__imageonebox">
							<div class="exhibition__imageone">
								<img class="exhibition__img" src="<?php the_post_thumbnail_url(); ?>" alt="">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php } /* конец while */ ?>

	<?php
	} // конец if
	else 
		echo "<h2>Записей нет.</h2>"; ?>


<?php get_footer() ?>