<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: lkvisitor-personaldata
 */
get_header(); ?>



<section class="lkvisitor">
	<div class="container_fluid">
		<div class="lkvisitor__row row">
			<?php get_sidebar(); ?>
			<div class="content">
				<div class="content__box lkPersonalData__content__box">
					<div class="lkDownloadTicket lkPersonalData js_parent_form">
						<h2 class="titleAvg lkvisitor__titleAvg">
							Личные данные
							<p class="lkPersonalData__edit js_btn_edit">редактировать</p>
						</h2>
						<form class="lkPersonalData__form js_form_edit">
							<div class="lkPersonalData__row row">
								<div class="lkPersonalData__col">
									<ul class="lkPersonalData__list">
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Название организации:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">ПАО Росгоснацснабкабель</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Фамилия:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Кириллов</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Имя:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Сергей</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Должность:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Коммерческий директор</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Телефон:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">+7 951 284 57 21</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												E-mail (не редактируется):
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea">kirillovsergei@rosgos.ru</textarea>
										</li>
									</ul>
								</div>
								<div class="lkPersonalData__col">
									<ul class="lkPersonalData__list">
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Сайт:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">http://web.com</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Страна:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Российская Федерация</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Город:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Москва</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Юридический адрес:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Россия, 190086, Ленинградская обл., г. Санкт-Петербург, ул. Инструмен-тальная, а/я 521</textarea>
										</li>
										<li class="lkPersonalData__item">
											<h4 class="lkPersonalData__subtitle">
												Сфера деятельности:
											</h4>
											<textarea spellcheck="false" disabled  class="autosize lkPersonalData__textarea js_edit_field">Производство строительных смесей, поставки цемента</textarea>
										</li>
										
									</ul>
								</div>
							</div>
							<h2 class="titleAvg lkvisitor__titleAvg">
								Сменить пароль
							</h2>
							<div class="popup__row row">
								<div class="popup__col">
									<input type="password" placeholder="Новый пароль" class="field__input">
								</div>
								<div class="popup__col">
									<input type="password" placeholder="Повторите ввод пароля" class="field__input">
								</div>
								<div class="popup__col">
									<button class="btn">
										<span>
											Сохранить
										</span>
									</button>
								</div>
							</div>
							<!-- /.row -->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container_fluid -->
	
</section>

<?php get_footer() ?>