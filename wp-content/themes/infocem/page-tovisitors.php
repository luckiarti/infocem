<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: to visitors
 */
get_header(); ?>

	<header class="headerParticipants">
		<div class="container_fluid">
			<div class="headerParticipants__wrapp">
				<div class="headerParticipants__row row">
					<div class="headerParticipants__col headerParticipants__col_left">
						<h2 class="titleAvg">
							Посетителям выставки
						</h2>
						<p class="abouTheExhibition__infotxt">
							Ежегодно на одной площадке собираются производители оборудования для производства цемента, бетона, железобетонных изделий, сухих строительных смесей, добавок и заполнителей, заводы ДСК, КПД. 
						</p>
						<p class="abouTheExhibition__infotxt">
							Выставка подкреплена одной из лучших деловых программ Европы, включающую выступления зарубежных экспертов строительной отрасли Европы, Азии и Ближнего Востока, кофе-брейки, обеды, круглый стол для детального обсуждения конкретных вопросов, а также интереснейшую культурно-развлекательную программу.
						</p>
						<p class="abouTheExhibition__infotxt last_margin">
							На этой выставке Вас ждут:
						</p>
					</div>
					<!-- /.headerParticipants__col -->
					<div class="headerParticipants__col headerParticipants__col_right">
						<div class="headerParticipants__image cube">
							<img class="headerParticipants__img" src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt="">
						</div>
					</div>
					<!-- /.headerParticipants__col -->
				</div>
				<!-- /.headerParticipants__row row -->
				<div class="headerParticipants__row row">
					<ul class="headerParticipants__list justi_cont">
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Крупнейшая в России
								</h3>
								<p class="blockSignatureLine__text">
									«Цемент. Бетон. Сухие смеси» — это крупнейшая в России специализированная выставка
									по данной тематике, которая ежегодно собирает <strong>более 6000 специалистов!</strong>
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Уникальные участники
								</h3>
								<p class="blockSignatureLine__text">
									Вы будете иметь возможность показать свою продукцию огромному количеству клиентов, специалистов и экспертов отрасли
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Насыщенная программа
								</h3>
								<p class="blockSignatureLine__text">
									Самая насыщенная деловая программа в Европе, уникальные программы конференций и семинаров, более 500 делегатов!
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Международные специалисты
								</h3>
								<p class="blockSignatureLine__text">
									Участники и посетители из <strong>30 стран мира!</strong>
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									Демонстрация техники
								</h3>
								<p class="blockSignatureLine__text">
									Демонстрация техники, крупногабаритных машин и механизмов прямо на выставочной площадке!
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
						<li class="headerParticipants__item">
							<div class="blockSignatureLine">
								<h3 class="blockSignatureLine__title">
									20 лет вместе с вами
								</h3>
								<p class="blockSignatureLine__text">
									Мероприятие с 20-летним стажем, которое знают и любят специалисты. Приходите, и Вы поймете, как провести 3 дня по-настоящему эффективно!
								</p>
							</div>
							<!-- /.blockSignatureLine -->
						</li>
					</ul>
					<!-- /.headerParticipants__list -->
					<div class="tac ">
						<a href="" class="btn headerParticipants__btn">
							<span>Оформить пригласительный билет</span>
						</a>
					</div>
				</div>
				<!-- /.headerParticipants__row row -->
			</div>
			<!-- /.headerParticipants__wrapp -->
		</div>	
		<!-- /.container_fluid -->
	</header>
	<!-- /.headerConference -->
	<section class="timeMap">
		<div class="container_fluid">
			<div class="timeMap__wrapp">
				<div class="timeMap__row row">
					<div class="timeMap__col">
						<div class="timeMap__desc">
							<h2 class="titleAvg">
								Время работы выставки
							</h2>

							<p class="timeMap__smalltitle">
								Дата проведения выставки
							</p>
							<ul class="timeMap__list">
								<li class="timeMap__item">
									28-30 ноября 2018 года
								</li>
							</ul>
							<p class="timeMap__smalltitle">
								Адрес павильона
							</p>
							<ul class="timeMap__list">
								<li class="timeMap__item">
									г. Москва, Краснопресненская наб. д. 14, ЦВК «Экспоцентр», Павильон 3, Станция метро «Выставочная»
								</li>
							</ul>
							<p class="timeMap__smalltitle">
								Часы работы
							</p>
							<ul class="timeMap__list">
								<li class="timeMap__item">
									28 ноября 10:00 – 18:00
								</li>
								<li class="timeMap__item">
									29 ноября 10:00 – 18:00
								</li>
								<li class="timeMap__item">
									30 ноября 10:00 – 16:00
								</li>
							</ul>
							<a href="" class="workingHours__download">
								Посмотреть схему проезда на выставку
							</a>
						</div>
					</div>
					<div class="timeMap__col">
						<div class="timeMap__map">
							<section id="cd-google-map" class="googlemap">
								<div id="google-container" class="google-container"></div>
								<div class="container googlemap__container">
								</div>
							</section>
							<!-- /googlemap -->
						</div>
					</div>
				</div>
				<!-- /.timeMap__row row -->
			</div>
			<!-- /.timeMap__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.timeMap -->
	<section class="listparticipants">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Список участников
			</h2>
			<div class="listparticipants__slider">
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
				<div class="listparticipants__slide">
					<ul class="listparticipants__list justi_cont">
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
						<li class="listparticipants__item">
							<div class="listparticipants__box">
								<div class="listparticipants__logo">
									<img class="listparticipants__img" src="<?php theme_uri()?>/images/list-participants/logos.png" alt="">
								</div>
								<div class="listparticipants__desc">
									<div class="listparticipants__country">
										<span class="listparticipants__icon" style="background-image: url(<?php theme_uri()?>/images/flag-lang/russia.svg);">
											
										</span>
										<span class="listparticipants__name">
											Russian Federation
										</span>
									</div>
									<div class="listparticipants__company">
										ООО Импульс ФлексиХит
									</div>
									<a href="" class="listparticipants__link">
										о компании
									</a>
								</div>
							</div>
						</li>
					</ul>
					<!-- /.listparticipants__list -->
				</div>
			</div>
			<!-- /.listparticipants__slider -->
			<div class="listparticipants__panelslide board">
				<div class="listparticipants__slideleft"><span>ПРЕД</span></div>
				<p class="listparticipants__num">
					<span class="count">1</span>/
					<span class="quantity"></span>
				</p>
				<div class="listparticipants__slideright"><span>СЛЕД</span></div>
			</div>
			
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.listparticipants -->
	<section class="advertising hotelBooking">
		<div class="container_fluid">
			<div class="advertising__row row">

				<div class="advertising__col">
					<h2 class="titleAvg">
						Бронирование гостиниц для посетителей выставки
					</h2>
					<p class="abouTheExhibition__infotxt advertising__text hotelBooking__text">
						Вы можете увеличить количество посетителей на стенде компании и повысить эффективность вашего участия в выставке, используя уже подготовленные нами рекламные материалы.
					</p>
					<p class="abouTheExhibition__infotxt advertising__text hotelBooking__text hotelBooking__text_small">
						Цены указаны с учетом завтрака и трансфера до места проведения выставки и конференции. НДС  не облагаются. За ранний заезд (до 14.00) и поздний выезд (после 12.00) взимается дополнительная плата за проживание. Пожалуйста, указывайте время заезда и выезда. Предлагаем услуги по встрече  и проводам в аэропорту и на ж/д вокзалах: Аэропорт (ж/д вокзал) – Гостиница // Гостиница — Аэропорт (ж/д вокзал) — 3 300 рублей.
					</p>
				</div>
				<div class="advertising__col">
					<div class="banner hotelBooking__banner">
						<div class="banner__block banner__block_280px">
							<div class="banner_advertising">
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.advertising__row row -->
			<div class="advertising__row row">
				<div class="advertising__col">
					<div class="advertising__box justi_cont">
						<div class="justi_cont">
							<div class="hotelBooking__pricenum">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Стоимость номеров
								</h3>
								<ul class="advertising__list">
									<li class="advertising__item hotelBooking__item_head justi_cont">
										<span class="hotelBooking__type">Тип размещения</span>
										<span class="hotelBooking__price">Цена/сутки</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
								</ul>
							</div>
							<!-- /.hotelBooking__pricenum -->
							<div class="hotelBooking__gallery">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Галерея отеля
								</h3>
								<ul class="gallery">
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/1.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/1.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/2.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/2.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/3.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/3.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/4.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/4.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/5.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/6.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/7.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/7.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/8.jpg" data-fancybox="gallery">
											<img src="<?php theme_uri()?>/images/gallery/small/8.jpg" alt="">
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						<div class="tac">
							<a href="" class="btn invitingCustomers__btn hotelBooking__btn">
								<span>Получить бесплатный билет</span>
							</a>
						</div>
						
						
					</div>
				</div>
				<div class="advertising__col">
					<div class="advertising__box justi_cont">
						<div class="justi_cont">
							<div class="hotelBooking__pricenum">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Стоимость номеров
								</h3>
								<ul class="advertising__list">
									<li class="advertising__item hotelBooking__item_head justi_cont">
										<span class="hotelBooking__type">Тип размещения</span>
										<span class="hotelBooking__price">Цена/сутки</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Одноместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
									<li class="advertising__item hotelBooking__item_body justi_cont">
										<span class="hotelBooking__type">Двухместное улучшенное</span>
										<span class="hotelBooking__price">5 800 ₽</span>
									</li>
								</ul>
							</div>
							<!-- /.hotelBooking__pricenum -->
							<div class="hotelBooking__gallery">
								<h3 class="advertising__subtitle hotelBooking__subtitle">
									Галерея отеля
								</h3>
								<ul class="gallery">
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/1.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/1.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/2.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/2.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/3.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/3.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/4.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/4.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/5.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/6.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/6.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/7.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/7.jpg" alt="">
										</a>
									</li>
									<li class="gallery__item">
										<a class="gallery__pic" href="<?php theme_uri()?>/images/gallery/small/8.jpg" data-fancybox="gallery_right">
											<img src="<?php theme_uri()?>/images/gallery/small/8.jpg" alt="">
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						<div class="tac">
							<a href="" class="btn invitingCustomers__btn hotelBooking__btn">
								<span>Получить бесплатный билет</span>
							</a>
						</div>
						
						
					</div>
				</div>
				
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->

	<section class="advertising visaSvisitors">
		<div class="container_fluid">
			<div class="advertising__row row">
				<div class="advertising__col">
					<h2 class="titleAvg">
						Визы для посетителей выставки
					</h2>
					<p class="abouTheExhibition__infotxt advertising__text visaSvisitors__text">
						Все иностранные граждане должны получить визу для въезда на территорию Российской Федерации. Условия для получения российской визы могут меняться в зависимости от страны проживания иностранного гражданина, поэтому свяжитесь с посольством Российской Федерации в вашей стране, чтобы получить точную информацию о необходимых документах.
					</p>
					<p class="abouTheExhibition__infotxt advertising__text visaSvisitors__text">
						Наша компания может предоставить иностранным участникам выставок и конференций визовую поддержку для получения деловой или туристической визы в Российскую Федерацию.
					</p>
				</div>
				<div class="advertising__col">
					<div class="visaSvisitors__box">
						<div class="visaSvisitors__image cube_left_bottom">
							<img class="visaSvisitors__img" src="<?php theme_uri()?>/images/passport.jpg" alt="">
						</div>
					</div>
					
				</div>
			</div>
			<!-- /.advertising__row row -->
			<div class="advertising__row visaSvisitors__visainfo row">
				<div class="advertising__col">
					<h2 class="titleAvg visaSvisitors__titleAvg orange_color">
						Деловая виза
					</h2>
					<div class="visaSvisitors__desc">
						<h3 class="visaSvisitors__subtitle">
							Для Граждан других стран
						</h3>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для получения деловой Российской визы требуется официальное приглашение Федеральной Миграционной Службы или Министерства Иностранных Дел РФ. 
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
						Стоимость приглашения составляет <strong>25 евро.</strong> 
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Подготовить Официальное приглашение займет <strong>от 14 до 18 дней</strong>, поэтому рекомендуем Вам подготовить и отправить все необходимые документы <strong>
								до 20 октября 2018 года. 
							</strong>
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Пакет документов включает Заявку на Официальное приглашение  и копию паспорта с фотографией. В зависимости от страны консульство может требовать копию или оригинал приглашения. Свяжитесь с вашим консульством для получения более подробной информации. В случае необходимости вы можете заказать курьерскую доставку через нашу компанию или самостоятельно.

						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
						Для оформления Официального приглашения гражданам других стран, Вам необходимо:
						</p>
						<ul class="participantsMixBild__list visaSvisitors__list">
							<li class="participantsMixBild__item">
								Прислать копию страницы паспорта с фотографией
							</li>
							<li class="participantsMixBild__item">
								Заполнить Заявку на официальное приглашение
							</li>
						</ul>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для некоторых стран (например, Китай, Индия и др.) доступен только этот вид приглашения. Для получения более подробной информации, пожалуйста, <a href="">свяжитесь с нами</a>.
						</p>
					</div>
					
				</div>
				<div class="advertising__col">
					<h2 class="titleAvg visaSvisitors__titleAvg transparent">
						txt
					</h2>
					<div class="visaSvisitors__desc">
						<h3 class="visaSvisitors__subtitle">
							Для граждан Ес и норвегии
						</h3>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для граждан ЕС и Норвегии (кроме Великобритании и Ирландии)  мы бесплатно предоставляем копию приглашения для получения деловой визы. В большинстве случаев для получения визы гражданами вышеупомянутых стран достаточно копии приглашения. Однако консульство оставляет за собой право затребовать оригинал приглашения. В этом случае вы можете заказать курьерскую доставку через нашу компанию или самостоятельно.
						</p>
						<p class="abouTheExhibition__infotxt visaSvisitors__infotext">
							Для оформления Официального приглашения Вам необходимо прислать <strong>копию страницы паспорта с фотографией</strong>.
						</p>
						
						
						
					</div>
					
				</div>
			</div>
			<!-- /.advertising__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.advertising -->
	<section class="mixBildBlock participate bookTicket" style="background-image: url(<?php theme_uri()?>/images/bg/bg-participate.jpg);">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="participate__title">
								Заказать пригласительный билет
							</h2>
							<p class="participate__text">
							Такое событие определенно стоит посетить. Мы ждём Вас на юбилейном XX <br> Форуме ЦБСС 28–30 ноября 2018 года в Москве, ЦВК «Экспоцентр» Павильон №3.
							</p>
							<a href="" class="btn btn_w260">
								<span>
									Заказать билет
								</span>
							</a>
						</div>
					</div>
				
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock -->

<?php get_footer() ?>