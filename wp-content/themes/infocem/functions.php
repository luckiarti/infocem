<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */


function theme_uri()
{
	echo get_template_directory_uri();
}

register_nav_menus(array(
	'top' => 'Верхнее',
	'bottom' => 'Внизу'
));

add_theme_support('post-thumbnails');
// set_post_thumbnail_size(250, 150);
// add_image_size('big-thumb', 400, 400, true);


add_action('wp_footer', 'add_scripts');
if (!function_exists('add_scripts')) {
	function add_scripts() {
	    if(is_admin()) return false;
	    wp_deregister_script('jquery');
	    wp_enqueue_script('start', get_template_directory_uri().'/start.min.js','','',true);
	    wp_enqueue_script('end', get_template_directory_uri().'/end.min.js','','',true);

	    if (is_page(array('posetitelyam', 'kontakty')) ) {
			  wp_enqueue_script('maps','https://maps.googleapis.com/maps/api/js?key=AIzaSyALAFNSXFHoT_G7dGW26MmaBi8okSVxOho','','',true);
			  wp_enqueue_script('mapSetting', get_template_directory_uri().'/js/map.js','','',true);
		}
	}
}

add_action('wp_print_styles', 'add_styles');
if (!function_exists('add_styles')) {
	function add_styles() {
	    if(is_admin()) return false;
		wp_enqueue_style( 'style', get_template_directory_uri().'/style.css' );
	}
}




// Отчеты

add_action( 'init', 'exhibition_reports' );
function exhibition_reports() {
  $labels = array(
    'name' => 'Отчеты',
    'singular_name' => 'Отчет',
    'add_new' => 'Добавить отчеты',
    'add_new_item' => 'Добавить новый отчет',
    'edit_item' => 'Редактировать отчет',
    'new_item' => 'Новый отчет',
    'all_items' => 'Все отчеты',
    'view_item' => 'Смотрите отчеты на сайте',
    'search_items' => 'Найти отчет',
    'not_found' =>  'Отчет не найден',
    'not_found_in_trash' => 'В корзине нет отчетов',
    'menu_name' => 'Отчеты'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true,
    'has_archive' => true, 
    'menu_icon' => 'dashicons-id',
    'menu_position' => 5,
    'supports' => array( 'title', 'editor')
  );
  register_post_type('reports', $args);
}


// Отзыв

add_action( 'init', 'reviews_human' );
function reviews_human() {
  $labels = array(
    'name' => 'Отзывы',
    'singular_name' => 'Отчет',
    'add_new' => 'Добавить отчеты',
    'add_new_item' => 'Добавить новый отчет',
    'edit_item' => 'Редактировать отчет',
    'new_item' => 'Новый отчет',
    'all_items' => 'Все отчеты',
    'view_item' => 'Смотрите отчеты на сайте',
    'search_items' => 'Найти отчет',
    'not_found' =>  'Отчет не найден',
    'not_found_in_trash' => 'В корзине нет отчетов',
    'menu_name' => 'Отзывы'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true,
    'has_archive' => true, 
    'menu_icon' => 'dashicons-id',
    'menu_position' => 5,
     'supports' => array( 'title', 'editor', 'thumbnail' )
  );
  register_post_type('reviews', $args);
}




// Пагинация

if (!function_exists('pagination')) {
  function pagination() {
    global $wp_query;
    $big = 999999999;
    $eyer = 2013;
    $links = paginate_links(array(
      'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
      'format' => '?paged=%#%',
      'current' => max(1, get_query_var('paged')),
      'type' => 'array',
      'prev_next' => false,
      // 'prev_text'    => '',
      //   'next_text'    => '',
      'total' => $wp_query->max_num_pages,
      'show_all'     => false,
      'end_size'     => 15,
      'mid_size'     => 15,
      'add_args'     => false,
      'add_fragment' => '',
      'before_page_number' => '<span>',
      'after_page_number' => '</span>'
    ));
     if( is_array( $links ) ) {
        echo '<ul class="exhibition__listdata">';
        foreach ( $links as $link ) {
          if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>".$link." ".$eyer++."</li>";
            else echo "<li>".$link." ".$eyer++."</li>"; 
        }
         echo '</ul>';
     }
  }
}




add_filter('excerpt_more', function($more) {
  return '...';
});