<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: news
 */
get_header(); ?>
	
	<section class="news">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Последние новости
			</h2>
			<div class="news__row row">

				


				<?php  
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				  $args = array(
			        'post_type' => 'post',
			        // 'meta_query' => array(
			        //   array(
			        //     'key' => 'type_video',
			        //     'value' => $type
			        //   )
			        // ),
			        'paged' => $paged,
			        'offset' => 0,
			        'posts_per_page' => 5,
			        'orderby' => 'modified',
			        'order' => 'ASC'
			      );
			      $wp_query = new WP_Query( $args );
			      while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
			     ?>

			     
			     
			    
				<div class="news__col">
					<a href="<?php the_permalink(); ?>" class="blockSignatureLine">
						<p class="blockSignatureLine__data">
							<?php the_time('d.m.Y'); ?>
						</p>
						<h3 class="blockSignatureLine__title">
							<?php the_title(); ?>
						</h3>
						<p class="blockSignatureLine__text">
							 <?php the_excerpt(); ?>
						</p>
					</a>
					<!-- /.blockSignatureLine -->
				</div>

				 <?php  endwhile;
				      wp_reset_postdata();

				   ?>
				

			</div>
			<!-- /.news__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.news -->
	

<?php get_footer() ?>