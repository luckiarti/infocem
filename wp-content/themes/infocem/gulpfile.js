var gulp = require('gulp');
var cssScss = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglyfly = require('gulp-uglyfly');
var babel = require('gulp-babel');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');


gulp.task('scssConcat', () => {
    return gulp.src('scss/style.scss')
        .pipe(sourcemaps.init())
            .pipe(cssScss().on('error', cssScss.logError))
            .pipe(cleanCSS())
        .pipe(sourcemaps.write('./scss'))
        .pipe(gulp.dest('./'));
});

var scriptsStart = [
    './js/jquery-3.3.1.min.js',
    './js/jquery-modal-video.js',
    // modal-video^
    './js/jquery.fancybox.min.js',
    './js/gallery.js',
    // fancybox^
];
var scriptsEnd = [
    './js/personaldata.js',
    // personaldata^
    './js/jquery.autosize.min.js',
    './js/autosize.js',
    // autosize^
     './js/select.js',
    // select^
    './js/modal-popup.js',
    // modal-popup^
    './js/slick.min.js',
    './js/slider.js',
    // slider^
    './js/videoytube.js',
    // youtubevideo^
    './js/search.js',
    // search^
    './js/navigationfixedscroll.js',
    // navigationfixedscroll^
    './js/menu.js',
    // menu^ 
    './js/main.js',
    // main^
];
gulp.task('scriptsConcatStart', () => {
    return gulp.src(scriptsStart)
        .pipe(sourcemaps.init())
            .pipe(concat('start.min.js'))
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});
gulp.task('scriptsConcatEnd', () => {
    return gulp.src(scriptsEnd)
        .pipe(sourcemaps.init())
            .pipe(concat('end.min.js'))
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
    gulp.watch(['scss/*/*.scss', 'js/*.js'], gulp.series([
        'scssConcat',
        'scriptsConcatStart',
        'scriptsConcatEnd',
    ]));
})

