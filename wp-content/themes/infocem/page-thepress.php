<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: the press
 */
get_header(); ?>
	
	<header class="headerPressRepresentatives">
		<div class="container_fluid">
			<div class="headerPressRepresentatives__wrapp">
				<div class="headerPressRepresentatives__row row">
					<div class="headerPressRepresentatives__col">
						<h2 class="titleAvg">
							Представителям прессы
						</h2>
						<div class="headerPressRepresentatives__desc">
							<h3 class="headerPressRepresentatives__subtitle">
								Пресс релиз «Цемент.Бетон.Сухие смеси» 2018
							</h3>
							<p class="abouTheExhibition__infotxt">
								Ежегодно на одной площадке собираются производители оборудования для производства цемента, бетона, железобетонных изделий, сухих строительных смесей, добавок и заполнителей, заводы ДСК, КПД. 
							</p>
							<p class="abouTheExhibition__infotxt">
								Выставка подкреплена одной из лучших деловых программ Европы, включающую выступления зарубежных экспертов строительной отрасли Европы, Азии и Ближнего Востока, кофе-брейки, обеды, круглый стол для детального обсуждения конкретных вопросов, а также интереснейшую культурно-развлекательную программу ...
							</p>
							<a href="" class="workingHours__download">
								Читать полностью
							</a>
							<a href="" class="workingHours__download headerPressRepresentatives__download">
								Скачать
							</a>
						</div>
					</div>
					<div class="headerPressRepresentatives__col">
						<div class="headerPressRepresentatives__board">
							<div class="headerPressRepresentatives__desc">
								<h3 class="headerPressRepresentatives__subtitle">
									Аккредитация прессы
								</h3>
								<p class="abouTheExhibition__infotxt">
									Мы предлагаем различные варианты сотрудничества со средствами массовой информации. Чтобы получить аккредитацию на деловую программу, воспользуйтесь формой ниже
								</p>
								<form class="feedback headerPressRepresentatives__feedback">
									<div class="headerPressRepresentatives__rowin row">
										<div class="col">
											<div class="field">
												<input type="text" class="field__input" placeholder="Название издания">
											</div>
											<!-- /.field -->
										</div>
										<div class="col">
											<div class="field">
												<input type="text" class="field__input" placeholder="Телефон">
											</div>
											<!-- /.field -->
										</div>
										<div class="col">
											<div class="field">
												<input type="text" class="field__input" placeholder="ФИО">
											</div>
											<!-- /.field -->
										</div>
										<div class="col">
											<div class="field">
												<input type="text" class="field__input" placeholder="E-mail">
											</div>
											<!-- /.field -->
										</div>
										<div class="col">
											<button class="btn">
												<span>
													Оставить заявку
												</span>
											</button>
										</div>
										<div class="col">
											<p class="field__agreement">
												Нажимая кнопку «Оставить заявку», вы соглашаетесь с <a href=""> политикой конфиденциальности </a> 
											</p>
										</div>
									</div>
								
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /.headerPressRepresentatives__row row -->
			</div>
			<!-- /.headerPressRepresentatives__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</header>
	<!-- /.headerConference -->
	<section class="bannersLogos">
		<div class="container_fluid">
			<h2 class="titleAvg">
				Представителям прессы
			</h2>
			<div class="headerPressRepresentatives__desc">
				<p class="abouTheExhibition__infotxt">
					Здесь Вы можете загрузить баннеры и логотипы для размещения
					на своём сайте. При размещении баннера обязательно проставляйте ссылку на соответствующий сайт! 
				</p>
			</div>
			<div class="bannersLogos__row row">
				<div class="bannersLogos__block">
					<div class="bannersLogos__rowinn row">
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/468х60px-RU.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/200х200-RU.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/160х160-RU.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/100х100-RU.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
					</div>
				</div>
				<div class="bannersLogos__block">
					<div class="bannersLogos__rowinn bannersLogos__rowinn_half">
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
						<div class="bannersLogos__banner">
							<img src="<?php theme_uri()?>/images/banner-logo/l_01.jpg" alt="">
							<p class="bannersLogos__textname">
								<a href="http://infocem.info/">http://infocem.info/</a>
							</p>
						</div>
					</div>
					
				</div>
			</div>
			<!-- /.bannersLogos__row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.bannersLogos -->

<?php get_footer() ?>