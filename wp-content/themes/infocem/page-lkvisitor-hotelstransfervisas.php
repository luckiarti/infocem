<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: lkvisitor-bookstand
 */
get_header(); ?>



<section class="lkvisitor">
	<div class="container_fluid">
		<div class="lkvisitor__row row">
			<?php get_sidebar(); ?>
			<div class="content">
				<div class="content__box lkDescription__content__box">
					<div class="lkDownloadTicket lkDescription">
						<h2 class="titleAvg lkvisitor__titleAvg">
							Забронировать стенд
						</h2>
						<p class="lkDescription__text">
							В рамках конференции в общей сложности выступили более 40 иностранных и отечественных специалистов. В мероприятии участвовали более 150 специалистов  из России, Азербайджана, Белоруссии, Германии, Грузии, Казахстана, Китая и других стран.
							<br>
							<br>
							В их числе — представители таких крупных компаний,как«БАСФ Строительные системы»,«Газпромнефть — Битумные материалы»,«ВакерХеми Рус», «ТехноНИКОЛЬ — Строительные системы», STUVAe.V., WBIGmbH, «Триада-Холдинг», «Холсим (Рус) Строительные материалы»,«Растро», «Сен-Гобен Строительная Продукция Рус»  и другие. 
						</p>
						<a href="" class="btn lkDescription__btn">
							<span>Связаться с менеджером</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container_fluid -->
	
</section>

<?php get_footer() ?>