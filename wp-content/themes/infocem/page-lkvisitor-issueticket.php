<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: lkvisitor-issueticket
 */
get_header(); ?>



<section class="lkvisitor">
	<div class="container_fluid">
		<div class="lkvisitor__row row">
			<?php get_sidebar(); ?>
			<div class="content">
				<div class="content__box">
					<h2 class="titleAvg lkvisitor__titleAvg">
						Оформить билет на выставку
					</h2>
					<div class="lkDownloadTicket issueticket">
						<div class="lkDownloadTicket__box">
							<div class="lkDownloadTicket__image">
								<img src="<?php theme_uri()?>/images/logotype/infcem.png" alt="">
							</div>
							<div class="lkDownloadTicket__desc">
								<h4 class="lkDownloadTicket__subtitle">
									ХХ Международный строительный форум «ЦЕМЕНТ. БЕТОН. СУХИЕ СМЕСИ»
								</h4>
								<span class="lkDownloadTicket__data">
									28-30 ноября 2018 года  |  Москва. Экспоцентр
								</span>
							</div>
						</div>
						<!-- /.lkDownloadTicket__titlebox -->
						<form action="" class="popup__form issueticket__form">
							<div class="popup__row row">
								<div class="popup__col">
									<input type="text" placeholder="Название организации" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Сайт" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Фамилия" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Страна" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Имя" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Город" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Должность" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Юридический адрес" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Телефон" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="Сфера деятельности" class="field__input">
								</div>
								<div class="popup__col">
									<input type="text" placeholder="E-mail" class="field__input">
								</div>
								<div class="popup__col">
									<button class="btn">
										<span>
											Оставить заявку
										</span>
									</button>
								</div>
							</div>
							<!-- /.row -->
						</form>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- /.container_fluid -->
	
</section>

<?php get_footer() ?>