<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: contacts
 */
get_header(); ?>
	
	<section class="contacts">
		<div class="container_fluid">
			
			<div class="contacts__wrapp">
				<div class="contacts__row row">
					<div class="contacts__col contacts__col_left">
						<h2 class="titleAvg">
							Контакты организаторов
						</h2>
						<div class="contacts__people row">
							<div class="contacts__peoplecol">
								<div class="person contacts__person">
									<div class="person__personality">
										<div class="person__image cube cube_small">
											<img class="person__img" src="<?php theme_uri()?>/images/contact-organizers/contact-organizers-1.jpg" alt="">
										</div>
										<div class="person__data">
											<p class="person__name">
												Большаков Эдуард Логинович
											</p>
											<p class="person__status">
												Руководитель проекта к.т.н., председатель комитета по цементу, бетону, сухим смесям Российского союза строителей
											</p>
											<p class="person__status">
												info@alitinform.ru
											</p>
											<p class="person__status">
												+7 (812) 380-65-72 (доб. 201)
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="contacts__peoplecol">
								<div class="person contacts__person">
									<div class="person__personality">
										<div class="person__image cube cube_small">
											<img class="person__img" src="<?php theme_uri()?>/images/contact-organizers/contact-organizers-2.jpg" alt="">
										</div>
										<div class="person__data">
											<p class="person__name">
												Киселева Надежда
											</p>
											<p class="person__status">
												Участие в выставке
											</p>
											<p class="person__status">
												expo@alitinform.ru
											</p>
											<p class="person__status">
												+7 (812) 335-09-92 (доб. 207)
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="contacts__peoplecol">
								<div class="person contacts__person">
									<div class="person__personality">
										<div class="person__image cube cube_small">
											<img class="person__img" src="<?php theme_uri()?>/images/contact-organizers/contact-organizers-3.jpg" alt="">
										</div>
										<div class="person__data">
											<p class="person__name">
												Карранса Инна
											</p>
											<p class="person__status">
												Участие в конференциях
											</p>
											<p class="person__status">
												sub@alitinform.ru
											</p>
											<p class="person__status">
												+7 (812) 335-09-91 (доб. 208)
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="contacts__peoplecol">
								<div class="person contacts__person">
									<div class="person__personality">
										<div class="person__image cube cube_small">
											<img class="person__img" src="<?php theme_uri()?>/images/contact-organizers/contact-organizers-4.jpg" alt="">
										</div>
										<div class="person__data">
											<p class="person__name">
												Большакова Надежда Анатольевна
											</p>
											<p class="person__status">
												Общие вопросы
											</p>
											<p class="person__status">
												info@alitinform.ru
											</p>
											<p class="person__status">
												+7 (812) 380-65-72 (доб. 201)
											</p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						<!-- /.contacts__people -->
					</div>
					<div class="contacts__col contacts__col_right">
						<h2 class="titleAvg">
							Соц сети
						</h2>
						<ul class="social contacts__social">
							<li class="social__item">
								<a href="" class="social__link">
									<?php include "images/social/social-vk.svg"; ?>
								</a>
							</li>
							<li class="social__item">
								<a href="" class="social__link">
									<?php include "images/social/social-fc.svg"; ?>
								</a>
							</li>
							<li class="social__item">
								<a href="" class="social__link">
									<?php include "images/social/social-tw.svg"; ?>
								</a>
							</li>
							<li class="social__item">
								<a href="" class="social__link">
									<?php include "images/social/social-in.svg"; ?>
								</a>
							</li>
							<li class="social__item">
								<a href="" class="social__link">
									<?php include "images/social/social-yt.svg"; ?>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.contacts__row row -->
			</div>
			<!-- /.contacts__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.news -->
	<section class="timeMap schemeMap">
		<div class="container_fluid">
			<div class="timeMap__wrapp">
				<div class="timeMap__row row">
					<div class="timeMap__col">
						<div class="timeMap__desc">
							<h2 class="titleAvg">
								Схема проезда на выставку
							</h2>
							<p class="schemeMap__metro"><span>Улица 1905 года</span></p>
							<ul class="schemeMap__list">
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-bus.svg" alt="">
									<span>12</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-trolleybus.svg" alt="">
									<span>12,21,121,23</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-minibus.svg" alt="">
									<span>12,24,252,235</span>
								</li>
							</ul>
							<p class="schemeMap__metro"><span>Краснопресненская</span></p>
							<ul class="schemeMap__list">
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-bus.svg" alt="">
									<span>12</span>
								</li>
							</ul>
							<p class="schemeMap__metro"><span>Киевская</span></p>
							<ul class="schemeMap__list">
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-bus.svg" alt="">
									<span>12</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-trolleybus.svg" alt="">
									<span>12,21,121,23</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-minibus.svg" alt="">
									<span>12,24,252,235</span>
								</li>
							</ul>
							<p class="schemeMap__metro"><span>Кутузовская</span></p>
							<ul class="schemeMap__list">
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-bus.svg" alt="">
									<span>12</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-trolleybus.svg" alt="">
									<span>12,21,121,23</span>
								</li>
								<li class="schemeMap__item">
									<img src="<?php theme_uri()?>/images/icon/icon-minibus.svg" alt="">
									<span>12,24,252,235</span>
								</li>
							</ul>
						
							<a href="" class="workingHours__download">
								Условия парковки вокруг выставки
							</a>
						</div>
					</div>
					<div class="timeMap__col">
						<div class="timeMap__map">
							<section id="cd-google-map" class="googlemap">
								<div id="google-container" class="google-container"></div>
								<div class="container googlemap__container">
								</div>
							</section>
							<!-- /googlemap -->
						</div>
					</div>
				</div>
				<!-- /.timeMap__row row -->
			</div>
			<!-- /.timeMap__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.timeMap -->
	

<?php get_footer() ?>