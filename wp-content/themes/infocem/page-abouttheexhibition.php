<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: about the exhibition
 */
get_header(); ?>


<?php  
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	  $args = array(
        'post_type' => 'reports',
        // 'meta_query' => array(
        //   array(
        //     'key' => 'type_video',
        //     'value' => $type
        //   )
        // ),
        'paged' => $paged,
        'posts_per_page' => 1,
        'orderby' => 'modified',
        'order' => 'ASC'
      );
      $wp_query = new WP_Query( $args );
      while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
     ?>
   
   
<section class="exhibition">
	<div class="container_fluid">
		<h2 class="titleAvg">
			Отчёты по выставкам
		</h2>
		<?php pagination(); ?>
		<!-- /.exhibition__listdata -->
		<div class="exhibition__row row">

			<!-- Gallery Foto-->
			<?php 
				$paste_gallery_one = get_post_meta( $post->ID, 'paste_gallery_one', false );
				$paste_gallery_two = get_post_meta( $post->ID, 'paste_gallery_two', false );
				$paste_gallery_three = get_post_meta( $post->ID, 'paste_gallery_three', false );
				$paste_gallery_four = get_post_meta( $post->ID, 'paste_gallery_four', false );
				$paste_gallery_five = get_post_meta( $post->ID, 'paste_gallery_five', false );
				$paste_gallery_six = get_post_meta( $post->ID, 'paste_gallery_six', false );
				$paste_gallery_seven = get_post_meta( $post->ID, 'paste_gallery_seven', false );
				$paste_gallery_eight = get_post_meta( $post->ID, 'paste_gallery_eight', false );
			?>

			<!--TEXT-->
			<?php 
				$paste_text_one = get_post_meta( $post->ID, 'paste_text_one', true );
				$paste_text_two = get_post_meta( $post->ID, 'paste_text_two', true );
				$paste_text_three = get_post_meta( $post->ID, 'paste_text_three', true );
				$paste_text_four = get_post_meta( $post->ID, 'paste_text_four', true );
				$paste_text_five = get_post_meta( $post->ID, 'paste_text_five', true );
				$paste_text_six = get_post_meta( $post->ID, 'paste_text_six', true );
				$paste_text_seven = get_post_meta( $post->ID, 'paste_text_seven', true );
				$paste_text_eight = get_post_meta( $post->ID, 'paste_text_eight', true );
				$paste_text_nine = get_post_meta( $post->ID, 'paste_text_nine', true );
				$paste_text_ten = get_post_meta( $post->ID, 'paste_text_ten', true );
			?>

			<!--paste_text_thank-->
			<?php 
				$paste_text_thank = get_post_meta( $post->ID, 'paste_text_thank', true );
			?>

			<!--FOTO-->
			<?php 
				$paste_foto_one = get_post_meta( $post->ID, 'paste_foto_one', true );
				$paste_foto_two = get_post_meta( $post->ID, 'paste_foto_two', true );
				$paste_foto_three = get_post_meta( $post->ID, 'paste_foto_three', true );
				$paste_foto_four = get_post_meta( $post->ID, 'paste_foto_four', true );
			?>

			<?php 
				// reviews
				$paste_reviews_one = get_post_meta( $post->ID, 'paste_reviews_one', true );

				$paste_reviews_two = get_post_meta( $post->ID, 'paste_reviews_two', true );
				$paste_reviews_two_two = get_post_meta( $post->ID, 'paste_reviews_two_two', true );

				$paste_reviews_three = get_post_meta( $post->ID, 'paste_reviews_three', true );

				$paste_reviews_four = get_post_meta( $post->ID, 'paste_reviews_four', true );

				$paste_reviews_five = get_post_meta( $post->ID, 'paste_reviews_five', true );

				$paste_reviews_six = get_post_meta( $post->ID, 'paste_reviews_six', true );
			?>
			<div class="exhibition__col">
				<h3 class="exhibition__title">
					<?php the_title(); ?>
				</h3>
				 <?php the_content(); ?>
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__video">
					<?php echo get_post_meta( $post->ID, 'paste_video', true ); ?>
				</div>
				
				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_one,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="person exhibition__person">
					<div class="person__personality">
						<div class="person__image">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="person__data">
							<p class="person__name">
								<?php the_title(); ?>
							</p>
							<p class="person__status">
								<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
							</p>
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
				
			</div>
		</div>
		<!-- /.container_row row -->
	
		<div class="exhibition__row row">
			<div class="exhibition__col" >
				<div class="exhibition__slider">
					<? foreach($paste_gallery_one as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slide">
						<div class="exhibition__slidebox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>

					<?php }} ?>
				</div>
				<div class="exhibition__sliderdost">
					<? foreach($paste_gallery_one as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slidedost slick-current">
						<div class="exhibition__slidedostbox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>
					<?php }} ?>
				</div>
				
			</div>
			<div class="exhibition__col">
				
				
				<?php
				 	echo $paste_text_one;
				 ?>
				
				 
			</div>
		</div>
		<!-- /.container_row row -->


		<div class="exhibition__row row">

				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_two,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="exhibition__col">
					<div class="person exhibition__person">
						<div class="person__personality">
							<div class="person__image">
								<?php the_post_thumbnail(); ?>
							</div>

							<div class="person__data">
								<p class="person__name">
									<?php the_title(); ?>
								</p>
								<p class="person__status">
									<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
								</p>
								<?php the_content(); ?>
							</div>
							
						</div>
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
				  <?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_two_two,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="exhibition__col">
					<div class="person exhibition__person">
						<div class="person__personality">
							<div class="person__image">
								<?php the_post_thumbnail(); ?>
							</div>

							<div class="person__data">
								<p class="person__name">
									<?php the_title(); ?>
								</p>
								<p class="person__status">
									<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
								</p>
								<?php the_content(); ?>
							</div>
							
						</div>
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
			
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">

			<div class="exhibition__col">
				<?php
				 	echo $paste_text_two;
				 ?>
				
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__imageonebox">
					<div class="exhibition__imageone">
						<img class="exhibition__img"  src="<?php echo wp_get_attachment_url($paste_foto_one);?>" alt="">
					</div>
				</div>
				
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col" >
				<div class="exhibition__slider">
					<? foreach($paste_gallery_two as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slide">
						<div class="exhibition__slidebox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>

					<?php }} ?>
				</div>
				<div class="exhibition__sliderdost">
					<? foreach($paste_gallery_two as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slidedost slick-current">
						<div class="exhibition__slidedostbox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>
					<?php }} ?>
				</div>
				
			</div>
			<div class="exhibition__col">
				
				
				<?php
				 	echo $paste_text_three;
				 ?>
				
				 
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_four;
				 ?>
				
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__rowin row">
					<ul class="exhibition__listimg">
						<? foreach($paste_gallery_three as $key) {?>
						<? foreach($key as $keyIn) {?>
							<li class="exhibition__itemimg">
								<div class="exhibition__imagemulti">
									<img class="exhibition__img"  src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
								</div>
							</li>
						<?php }} ?>
						
						
					</ul>
					
				</div>
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col">
				<div class="exhibition__rowin row">
					<ul class="exhibition__listimg">
						<li class="exhibition__itemimg exhibition__itemimg_big">
							<div class="exhibition__imagemulti">
								<img class="exhibition__img"  src="<?php echo wp_get_attachment_url( $paste_foto_two ); ?>" alt="">
							</div>
						</li>
						<? foreach($paste_gallery_four as $key) {?>
						<? foreach($key as $keyIn) {?>
							<li class="exhibition__itemimg">
								<div class="exhibition__imagemulti">
									<img class="exhibition__img"  src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
								</div>
							</li>
						<?php }} ?>
						
						
					</ul>
					
				</div>
			</div>
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_five;
				 ?>
				
			</div>
		</div>
		<!-- /.container_row row -->
		<div class="exhibition__row row">
			
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_six;
				 ?>
				
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__slider">
					<? foreach($paste_gallery_five as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slide">
						<div class="exhibition__slidebox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>

					<?php }} ?>
				</div>
				<div class="exhibition__sliderdost">
					<? foreach($paste_gallery_five as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slidedost slick-current">
						<div class="exhibition__slidedostbox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>
					<?php }} ?>
				</div>

				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_three,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="person exhibition__person">
					<div class="person__personality">
						<div class="person__image">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="person__data">
							<p class="person__name">
								<?php the_title(); ?>
							</p>
							<p class="person__status">
								<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
							</p>
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col">
				<div class="exhibition__rowin row">
					<ul class="exhibition__listimg">
						<? foreach($paste_gallery_six as $key) {?>
						<? foreach($key as $keyIn) {?>
						<li class="exhibition__itemimg">
							<div class="exhibition__imagemulti">
								<img class="exhibition__img"  src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
							</div>
						</li>
						<?php }} ?>
					</ul>
					
				</div>
				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_four,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="person exhibition__person">
					<div class="person__personality">
						<div class="person__image">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="person__data">
							<p class="person__name">
								<?php the_title(); ?>
							</p>
							<p class="person__status">
								<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
							</p>
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
			</div>
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_seven;
				 ?>
				
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_eight;
				 ?>
				
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__imageonebox">
					<div class="exhibition__imageone">
						<img class="exhibition__img" src="<?php echo wp_get_attachment_url( $paste_foto_three ); ?>" alt="">
						
					</div>
				</div>
				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_five,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="person exhibition__person">
					<div class="person__personality">
						<div class="person__image">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="person__data">
							<p class="person__name">
								<?php the_title(); ?>
							</p>
							<p class="person__status">
								<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
							</p>
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
				
			</div>
		</div>
		<!-- /.container_row row -->
		<div class="exhibition__row row">
			<div class="exhibition__col">
				<div class="exhibition__slider">
					<? foreach($paste_gallery_seven as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slide">
						<div class="exhibition__slidebox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>

					<?php }} ?>
				</div>
				<div class="exhibition__sliderdost">
					<? foreach($paste_gallery_seven as $key) {?>
					<? foreach($key as $keyIn) {?>
					<div class="exhibition__slidedost slick-current">
						<div class="exhibition__slidedostbox">
							<img src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</div>
					<?php }} ?>
				</div>
				<?php 
					 $args_reviews = array(
				        'post_type' => 'reviews',
				        'p' => $paste_reviews_six,
				        // 'meta_query' => array(
				        //   array(
				        //     'key' => 'type_video',
				        //     'value' => $type
				        //   )
				        // ),
				        'posts_per_page' => 1,
				        // 'orderby' => 'modified',
				        // 'order' => 'DESC'
				      );
				      $big_reviews = new WP_Query( $args_reviews );
				      while ( $big_reviews->have_posts() ) : $big_reviews->the_post(); 
				 ?>
				<div class="person exhibition__person">
					<div class="person__personality">
						<div class="person__image">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="person__data">
							<p class="person__name">
								<?php the_title(); ?>
							</p>
							<p class="person__status">
								<?php echo  get_post_meta( $post->ID, 'paste_position', true ); ?>
							</p>
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
				 <?php  endwhile;
				      wp_reset_postdata();

				  ?>
			</div>
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_nine;
				 ?>
				
			</div>
		</div>
		<!-- /.container_row row -->

		<div class="exhibition__row row">
			<div class="exhibition__col">
				<?php
				 	echo $paste_text_ten;
				 ?>
				
			</div>
			<div class="exhibition__col exhibition__col_order">
				<div class="exhibition__imageonebox">
					<div class="exhibition__imageone">
						<img class="exhibition__img" src="<?php echo wp_get_attachment_url( $paste_foto_four ); ?>" alt="">
					</div>
				</div>
				
			</div>
		</div>
		<!-- /.container_row row -->
		<div class="exhibition__row row exhibition__row_gallery">
			<div class="exhibition__rowin row">
				<ul class="exhibition__listimg">
					<? foreach($paste_gallery_eight as $key) {?>
					<? foreach($key as $keyIn) {?>
					<li class="exhibition__itemimg">
						<div class="exhibition__imagemulti">
							<img class="exhibition__img"  src="<?php echo wp_get_attachment_url( $keyIn ); ?>" alt="">
						</div>
					</li>
					<?php }} ?>
					
				</ul>
			</div>
		</div>
	<!-- /.container_row row -->

		<p class="exhibition__text exhibition__text_thanks">
			<?php
			 	echo $paste_text_thank;
			 ?>
		</p>
		
	</div>
	<!-- /.container_fluid -->
</section>
<!-- /.exhibition -->
 <?php  endwhile;
      wp_reset_postdata();

     ?>


<?php get_footer() ?>