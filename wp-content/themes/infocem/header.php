<?php 

	if (is_page('o-konferentsii')) {
		$bg_conference = 'bg_conference';
	}


 ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="keywords" content="">
		<link href="favicon.png" rel="shortcut icon" type="image/x-icon" />
		<title>Название</title>
		<?php wp_head(); ?>
	</head>
	<body id="body">
		<div id="wrapper" class="<?=$bg_conference?>">
			<div class="fixednavigation js_fixednavigation">
				<div class="container_fluid">
					<div class="fixednavigation__wrapp justi_cont">
						
						<ul class="fixednavigation__list">
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									О Выставке
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Тематики
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Организаторы
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Программа мероприятий
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Партнёры
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Фотоотчёт 2017
								</a>
							</li>
							<li class="fixednavigation__item">
								<a href="" class="fixednavigation__link">
									Отзывы
								</a>
							</li>
						</ul>
						<a href="#body" class="fixednavigation__scrolltop scrollto">
							<span class="text">Наверх</span>
						</a>
					</div>
					<!-- /.fixednavigation__wrapp -->
				
				</div>
				<!-- /.container_fluid -->
			</div>
			<nav class="navigation">
				<div class="container_fluid">
					
					<ul class="navigation__list justi_cont navigation__list__mobile">
						<li class="navigation__item">
							<div class="logo">
								<a href="/">
									<img src="<?php theme_uri()?>/images/logo/logo.svg" alt="">
								</a>
							</div>
						</li>
						<li class="navigation__item navigation__item_menu">
							<?php 
								$ark = array(
									'theme_location' => 'top',
									'menu'            => '', 
									'container'       => false, 
									'container_class' => '', 
									'container_id'    => '',
									'menu_class'      => 'menu', 
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 0,
									'walker'          => '',
								);
							?>
							<?php wp_nav_menu($ark); ?>
							<a href="" class="lang lang__mobile">
								<span class="lang__text">en</span>
								<span class="lang__flag" style="background-image: url(<?php theme_uri()?>/images/flag-lang/en.jpg);"></span>
							</a>
							
						</li>
						<li class="navigation__item">
							<ul class="navigation__list justi_cont iconav">
								<li class="navigation__item">
									<div class="search-block">
										<div class="search-wrapper">
										    <div class="input-holder">
										        <input type="text" class="search-input" placeholder="Найти" />
										        <button class="search-icon"><span></span></button>
										    </div>
										    <span class="search-close close"></span>
										</div>
									</div>
								</li>
								<li class="navigation__item">
									<a href="" class="iconav__person" style="background-image: url(<?php theme_uri()?>/images/icon/icon-person.svg);">
										
									</a>
								</li>
								<li class="navigation__item navigation__item_lang">
									<a href="" class="lang">
										<span class="lang__text">en</span>
										<span class="lang__flag" style="background-image: url(<?php theme_uri()?>/images/flag-lang/en.jpg);"></span>
									</a>
								</li>
							</ul>
							<div class="burger">
								<div class="burger__icon">
									<div class="burger__icon_line burger__icon_line_1"></div>
									<div class="burger__icon_line burger__icon_line_2"></div>
									<div class="burger__icon_line burger__icon_line_3"></div>
								</div>
								<!-- /.burger__icon -->
							</div>
							<!-- /.burger -->
							
						</li>
					</ul>
				</div>
				<!-- /.container_fluid -->
			</nav>
			<!-- /.navigation -->