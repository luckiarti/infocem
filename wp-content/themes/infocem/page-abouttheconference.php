<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: About the conference
 */
get_header(); ?>

	<header class="headerConference">
		<div class="container_fluid">
			<div class="headerConference__wrapp">
				<div class="headerConference__row row">
					<div class="headerConference__col headerConference__col_left">
						<div class="headerConference__box">
							<h2 class="headerConference__title">
								V Международная конференция «Индустриальное домостроение: <br> производство, проектирование, строительство»
							</h2>
							<img class="headerConference__img" src="<?php theme_uri()?>/images/logotype/block-read.png" alt="">
							<br>
							<div class="headerConference__center">
								<a href="" class="btn headerConference__btn">
									<span>Принять участие</span>
								</a>
							</div>
							<br>
							<div class="headerConference__center">
								<a href="" class="headerConference__download">
									Список докладчиков.pdf
								</a>
							</div>
							
						</div>
					</div>
					<div class="headerConference__col headerConference__col_right">
						<div class="headerConference__box">
							<div class="headerConference__box">
								<h2 class="headerConference__title">
									XX Международная научно-техническая конференция «Современные технологии сухих смесей в строительстве»
								</h2>
								<img class="headerConference__img" src="<?php theme_uri()?>/images/logotype/mix-build.png" alt="">
								<br>
								<div class="headerConference__center">
									<a href="" class="btn headerConference__btn">
										<span>Принять участие</span>
									</a>
								</div>
								<br>
								<div class="headerConference__center">
									<a href="" class="headerConference__download">
										Список докладчиков.pdf
									</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.headerConference__wrapp -->
		</div>	
		<!-- /.container_fluid -->
	</header>
	<!-- /.headerConference -->
	<section class="mixBildBlock conferenceTopicsMixBild mixBildBlock_padtop">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="titleAvg">
								Тематики конференции BlockRead 2018
							</h2>
							<ul class="sergininKonusu__list conferenceTopicsMixBild__list justi_cont">
								<li class="sergininKonusu__item">
									Заводы «под ключ»
								</li>
								<li class="sergininKonusu__item">
									Тара, упаковка, транспортировка
								</li>
								<li class="sergininKonusu__item">
									Добавки в бетон и цемент, пигменты, заполнители
								</li>
								<li class="sergininKonusu__item">
									Лабораторное и аналитическое оборудование
								</li>
								<li class="sergininKonusu__item">
									Сырье и оборудование для его подготовки
								</li>
								<li class="sergininKonusu__item">
									Цемент, известь, гипс
								</li>
								<li class="sergininKonusu__item">
									Силоса, смесители, дозаторы
								</li>
								<li class="sergininKonusu__item">
									Системы управления и контроля качества продукции
								</li>
								<li class="sergininKonusu__item">
									Энергоэффективные технологии и автоматизация в строительстве
								</li>
								<li class="sergininKonusu__item">
									Арматура и опалубка
								</li>
							</ul>
						</div>
					</div>
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="titleAvg">
								Тематики конференции MixBuild 2018
							</h2>
							<ul class="sergininKonusu__list conferenceTopicsMixBild__list justi_cont">
								<li class="sergininKonusu__item">
									Заводы «под ключ»
								</li>
								<li class="sergininKonusu__item">
									Тара, упаковка, транспортировка
								</li>
								<li class="sergininKonusu__item">
									Добавки в бетон и цемент, пигменты, заполнители
								</li>
								<li class="sergininKonusu__item">
									Лабораторное и аналитическое оборудование
								</li>
								<li class="sergininKonusu__item">
									Сырье и оборудование для его подготовки
								</li>
								<li class="sergininKonusu__item">
									Цемент, известь, гипс
								</li>
								<li class="sergininKonusu__item">
									Силоса, смесители, дозаторы
								</li>
								<li class="sergininKonusu__item">
									Системы управления и контроля качества продукции
								</li>
								<li class="sergininKonusu__item">
									Энергоэффективные технологии и автоматизация в строительстве
								</li>
								<li class="sergininKonusu__item">
									Арматура и опалубка
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock conferenceTopicsMixBild -->

	<section class="mixBildBlock keyFactsMixBild">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="titleAvg keyFactsMixBild__titleAvg">
								Основные факты о BlockRead 2018
							</h2>
							<div class="keyFactsMixBild__row row">
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											250+ участников
										</h3>
										<p class="blockSignatureLine__text">
											Более 250 участников из России, Китая, США, стран ЕС и СНГ
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											30+ докладчиков
										</h3>
										<p class="blockSignatureLine__text">
											36 докладчиков из 18 стран со всего мира
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											Выставка PreCast — 2018
										</h3>
										<p class="blockSignatureLine__text">
											Одна площадка со специализированной выставкой по тематике сборного железобетона PreCast — 2018
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											Технические экскурсии
										</h3>
										<p class="blockSignatureLine__text">
											Технические экскурсии на крупнейшие ДСК Москвы и Московской области
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
							</div>
							
						</div>
					</div>
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="titleAvg keyFactsMixBild__titleAvg">
								Основные факты о MixBuild 2018
							</h2>
							<div class="keyFactsMixBild__row row">
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											200 участников
										</h3>
										<p class="blockSignatureLine__text">
											2000 участников из России, Китая, США, стран ЕС и СНГ
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											30+ докладчиков
										</h3>
										<p class="blockSignatureLine__text">
											36 докладчиков из 18 стран со всего мира
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											Выставка PreCast — 2018
										</h3>
										<p class="blockSignatureLine__text">
											Одна площадка со специализированной выставкой по тематике сборного железобетона PreCast — 2018
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											Технические экскурсии
										</h3>
										<p class="blockSignatureLine__text">
											Технические экскурсии на крупнейшие ДСК Москвы и Московской области
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
								<div class="keyFactsMixBild__col keyFactsMixBild__col_full">
									<div class="blockSignatureLine">
										<h3 class="blockSignatureLine__title">
											СПЕЦИАЛЬНАЯ СЕКЦИЯ
										</h3>
										<p class="blockSignatureLine__text">
											«Современные добавки для сухих смесей» под председательством Йохана Планка, доктора наук, профессора Мюнхенского технического университета, Германия
										</p>
									</div>
									<!-- /.blockSignatureLine -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock keyFactsMixBild-->
	<section class="mixBildBlock participantsMixBild">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<div class="participantsMixBild__image">
								<img class="participantsMixBild__img" src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt="">
							</div>
							<h2 class="titleAvg">
								Участники BlockRead 2018
							</h2>
							<ul class="participantsMixBild__list">
								<li class="participantsMixBild__item">
									Руководители бетонных и железобетонных заводов
								</li>
								<li class="participantsMixBild__item">
									Технические специалисты в области химии и технологий бетона, современных вяжущих веществ, химических добавок, технологического оборудования
								</li>
								<li class="participantsMixBild__item">
									Представители региональных рынков строительных материалов
								</li>
								<li class="participantsMixBild__item">
									Аналитики мирового и региональных рынков строительных материалов
								</li>
								<li class="participantsMixBild__item">
									Все те, кто отвечает за организацию технологических процессов на предприятиях отрасли
								</li>
							</ul>
						</div>
					</div>
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<div class="participantsMixBild__image">
								<img class="participantsMixBild__img" src="<?php theme_uri()?>/images/foto-exhibition/2.jpg" alt="">
							</div>
							<h2 class="titleAvg">
								Участники MixBuild 2018
							</h2>
							<ul class="participantsMixBild__list">
								<li class="participantsMixBild__item">
									Руководители бетонных и железобетонных заводов
								</li>
								<li class="participantsMixBild__item">
									Технические специалисты в области химии и технологий бетона, современных вяжущих веществ, химических добавок, технологического оборудования
								</li>
								<li class="participantsMixBild__item">
									Представители региональных рынков строительных материалов
								</li>
								<li class="participantsMixBild__item">
									Аналитики мирового и региональных рынков строительных материалов
								</li>
								<li class="participantsMixBild__item">
									Все те, кто отвечает за организацию технологических процессов на предприятиях отрасли
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock -->
	<section class="presentationReport">
		<div class="container_fluid">
			<div class="presentationReport__row row">
				<div class="presentationReport__col">
					<div class="presentationReport__box">
						<h2 class="titleAvg">
							Выступление с докладом
						</h2>
						<p class="presentationReport__text">
							Участник конференции имеет возможность выступить с аналитическим или обзорным докладом, который может быть опубликован в Международном аналитическом обозрении «ALITinform: Цемент. Бетон. Сухие смеси» на бесплатной основе.
							<br>
							<br>
							Принимаются только аналитические и обзорные доклады. Время выступлении – не более 25 минут. Доклад и статью в электронном виде необходимо прислать в оргкомитет не позднее 1 августа 2018 г. Объем печатного варианта статьи до 10 страниц. Название статьи, ее авторы и аннотация должны быть представлены на русском и английском языках. Решение о приеме аналитических и обзорных докладов принимает программный комитет конференции.
						</p>
					</div>
				</div>
				<!-- /.container__col -->
				<div class="presentationReport__col presentationReport__col_order">
					<div class="presentationReport__box presentationReport__box_rightfoto">
						<div class="presentationReport__image cube_left_top">
							<img class="presentationReport__img" src="<?php theme_uri() ?>/images/foto-exhibition/2.jpg" alt="">
						</div>
					</div>
				</div>
				<!-- /.container__col -->
				<div class="presentationReport__col">
					<div class="presentationReport__box presentationReport__box_leftfoto ">
						<div class="presentationReport__image cube_right_bottom">
							<img class="presentationReport__img" src="<?php theme_uri() ?>/images/foto-exhibition/2.jpg" alt="">
						</div>
					</div>
				</div>
				<!-- /.container__col -->
				<div class="presentationReport__col">
					<div class="presentationReport__box presentationReport__box_righttext">
						<h2 class="titleAvg">
							Рекламные возможности
						</h2>
						<p class="presentationReport__text">
							Международные конференции — это отличная возможность рассказать о себе и своей продукции потенциальным клиентам. Мы можем предложить:
						</p>
						<ul class="participantsMixBild__list">
							<li class="participantsMixBild__item">
								Руководители бетонных и железобетонных заводов
							</li>
							<li class="participantsMixBild__item">
								Представители региональных рынков строительных материалов
							</li>
							<li class="participantsMixBild__item">
								Аналитики мирового и региональных рынков строительных материалов
							</li>
							<li class="participantsMixBild__item">
								Все те, кто отвечает за организацию технологических процессов на предприятиях отрасли
							</li>
						</ul>
					</div>
				</div>
				<!-- /.container__col -->
				
			</div>
			<!-- /.presentationReport__row row -->
		</div>
		<!-- /.container_fluid -->
	</section>
	<!-- /.presentationReport -->
	<section class="mixBildBlock participate" style="background-image: url(<?php theme_uri()?>/images/bg/bg-participate.jpg);">
		<div class="container_fluid">
			<div class="mixBildBlock__wrapp">
				<div class="mixBildBlock__row row">
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<h2 class="participate__title">
								Принять участие в конференции
							</h2>
							<p class="participate__text">
								Для участия в конференции необходимо оставить свои контактные данные в форме, после чего на указанный Вами e-mail будет отравлен файл для оформления заявки. 
							</p>
							<p class="participate__text margin_bottom">
								На конференции Вам будет доступно:
							</p>
							<ul class="participantsMixBild__list participate__list">
								<li class="participantsMixBild__item">
									Участие в рабочей части конференции
								</li>
								<li class="participantsMixBild__item">
									Портфель участника
								</li>
								<li class="participantsMixBild__item">
									Синхронный перевод
								</li>
								<li class="participantsMixBild__item">
									Кофе-брейки и обеды
								</li>
								<li class="participantsMixBild__item">
									Культурная программа
								</li>
								<li class="participantsMixBild__item">
									Бесплатный проход на выставку
								</li>
							</ul>
						</div>
					</div>
					<div class="mixBildBlock__col">
						<div class="mixBildBlock__box">
							<form class="feedback mixBildBlock__feedback">
								<div class="field">
									<input type="text" class="field__input field__input_transparent" placeholder="Ваше имя">
								</div>
								<!-- /.field -->
								<div class="field">
									<input type="text" class="field__input field__input_transparent" placeholder="Телефон">
								</div>
								<!-- /.field -->
								<div class="field">
									<input type="text" class="field__input field__input_transparent" placeholder="E-mail">
								</div>
								<!-- /.field -->
								<button class="btn feedback__btn">
									<span>
										Получить заявку на участие
									</span>
								</button>
								<p class="field__agreement">
									Нажимая кнопку «Получить заявку на участие», вы соглашаетесь с <a href=""> политикой конфиденциальности </a> 
								</p>
							</form>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.mixBildBlock__wrapp -->
		</div>
		<!-- /.container_fluid -->
	</section>	
	<!-- /.mixBildBlock -->






<?php get_footer() ?>