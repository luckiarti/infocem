$(document).ready(function() {
	var modalOpen = $('.modal-open');
	var modalPopup = $('.modal-popup');
	var modalClose = $('.modal-close');

	var body = $('body');

	// Open popup 
	modalOpen.click(function(event) {
		$(this).addClass('active');
		var dataModal = $(this).data('modal-target');
		// alert(dataModal);
		$("[data-modal-id='" + dataModal + "']").fadeIn();
		body.addClass('hidden');
	});

	// close popup
	body.click(function(e) {
		var targetBlock = $(e.target);
		var clickBurger;
		// проверяем был ли клик по гамбургеру
		if(targetBlock.has('.modal-open').length || targetBlock.closest('.modal-open').length) {
			clickBurger = true;
		}
		if(modalOpen.hasClass('active')) {
			if(targetBlock.closest('.modal-popup-block').length == 0 && !clickBurger) {
				console.log ('close tel__me');
				modalPopup.fadeOut();
				body.removeClass('hidden');
			}
		}
	});

	modalClose.click(function(event) {
		var modalPopup = $(this).closest('.modal-popup');
		modalPopup.fadeOut();
		body.removeClass('hidden');
	});
});