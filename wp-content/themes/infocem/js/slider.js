
$(document).ready(function() {
$('.slider__slider').slick({
   arrows:true,
   dots:true,
   slidesToShow: 1,
   slidesToScroll: 1,
   focusOnSelect: true,
   nextArrow: '<div class="slider__arrow slider__arrow_right" aria-hidden="true"></div>',
   prevArrow: '<div class="slider__arrow slider__arrow_left" aria-hidden="true"></div>',
   adaptiveHeight: true,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 730,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 530,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});

});

$(document).ready(function() {
$('.listparticipants__slider').slick({
   arrows:true,
   dots:false,
   slidesToShow: 3,
   slidesToScroll: 1,
   focusOnSelect: true,
   nextArrow: '<div class="listparticipants__arrow listparticipants__arrow_right" aria-hidden="true"></div>',
   prevArrow: '<div class="listparticipants__arrow listparticipants__arrow_left" aria-hidden="true"></div>',
   adaptiveHeight: true,
   responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 530,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});


});


$(document).ready(function() {
  var slicklist = $('.listparticipants__slider');
  var count = $('.count');
  var quantity = $('.quantity');
  var slideleft = $('.listparticipants__slideleft');
  var slideright = $('.listparticipants__slideright');
  slideleft.click(function(event) {
    $(this).parents('.listparticipants').find('.listparticipants__slider').slick('slickPrev');
  });
   slideright.click(function(event) {
    $(this).parents('.listparticipants').find('.listparticipants__slider').slick('slickNext');
  });


  slicklist.on('afterChange', function(event, slick, currentSlide){
        count.text(currentSlide + 1);
  });
  quantity.text(slicklist.slick("getSlick").slideCount);


});






$(document).ready(function() {
  // tabslide


    $(".exhibition__col .exhibition__slidedost").click(function() {
      var tthis = $(this).closest('.exhibition__col');
      tthis.find('.exhibition__slidedost').removeClass("slick-current").eq($(this).index()).addClass("slick-current");
      tthis.find('.exhibition__slide').hide().eq($(this).index()).fadeIn();
    }).eq(0).addClass("slick-current");
});