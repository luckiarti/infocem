$(document).ready(function() {
	

	var parentForm = $('.js_parent_form');
	var form = $('.js_form_edit');
	var btn = $('.js_btn_edit');
	btn.click(function(event) {
		var $this = $(this).closest('.js_parent_form');
		
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$(this).text('Сохранить');
			$this.find('.js_form_edit').addClass('active');
			$this.find('.js_edit_field').removeAttr('disabled');
			$('.autosize').autosize();
		}
		else {
			$(this).text('Редактировать');
			$('.autosize').autosize();
			$this.find('.js_form_edit').removeClass('active');
			$this.find('.js_edit_field').attr('disabled');
		}
		
	});

});