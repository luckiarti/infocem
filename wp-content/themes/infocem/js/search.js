$(document).ready(function() {
	$('.search-icon, .search-close').click(function(evt) { 
		var obj = this;
		var container = $(obj).closest('.search-wrapper'); 
        if(!container.hasClass('active')){
            container.addClass('active');
            evt.preventDefault();
        }
        else if(container.hasClass('active') && $(obj).closest('.input-holder').length == 0){
            container.removeClass('active');
            // clear input
            container.find('.search-input').val('');
        }
	});
	   
});