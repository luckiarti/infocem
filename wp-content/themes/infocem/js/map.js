jQuery(document).ready(function($){
	//set your google maps parameters
	var latitude = 55.752243, 
		longitude = 37.545114,
		map_zoom = 16;



	
	//google map custom marker icon - .png fallback for IE11
	var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
	var marker_url = ( is_internetExplorer11 ) ? 'https://cdn0.iconfinder.com/data/icons/app-tab-bar-icons-for-iphone/60/Marker_map_base_pin_market_pink_location_right_chartreuse_azure.png' : 'https://cdn0.iconfinder.com/data/icons/app-tab-bar-icons-for-iphone/60/Marker_map_base_pin_market_pink_location_right_chartreuse_azure.png';
		
	//define the basic color of your map, plus a value for saturation and brightness
	var	main_color = '#FF7910',
		saturation_value= -0,
		brightness_value= 5;


		
	//set google map options
	var map_options = {
      	center: new google.maps.LatLng(latitude, longitude),
      	zoom: map_zoom,
      	panControl: false,
      	zoomControl: false,
      	mapTypeControl: false,
      	streetViewControl: false,
      	mapTypeId: google.maps.MapTypeId.ROADMAP,
      	scrollwheel: false,
		
    }



    //inizialize the map
	var map = new google.maps.Map(document.getElementById('google-container'), map_options);
	//add a custom marker to the map				
	var marker = new google.maps.Marker({
	  	position: new google.maps.LatLng(latitude, longitude),
	    map: map,
	    visible: true,
	 	icon: marker_url,

	});







});

  