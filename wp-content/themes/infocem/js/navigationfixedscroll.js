$(document).ready(function() {
	$("a.scrollto").click(function () {
		var height = $('.js_fixednavigation').height();
        var elementClick = '#'+$(this).attr("href").split("#")[1]
        var destination = $(elementClick).offset().top - height;
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 500);
        return false;
    });


     $(window).scroll(function(){
       var height = 10;
        $('.fixednavigation').toggleClass('active', $(this).scrollTop() > height);
     });
});