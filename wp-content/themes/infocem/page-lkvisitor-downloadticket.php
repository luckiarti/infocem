<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: lkvisitor-downloadticket
 */
get_header(); ?>



<section class="lkvisitor">
	<div class="container_fluid">
		<div class="lkvisitor__row row">
			<?php get_sidebar(); ?>
			<div class="content">
				<div class="content__box">
					<h2 class="titleAvg lkvisitor__titleAvg">
						Скачать билет на выставку
					</h2>
					<div class="lkDownloadTicket">
						<div class="lkDownloadTicket__box">
							<div class="lkDownloadTicket__image">
								<img src="<?php theme_uri()?>/images/logotype/infcem.png" alt="">
							</div>
							<div class="lkDownloadTicket__desc">
								<h4 class="lkDownloadTicket__subtitle">
									ХХ Международный строительный форум «ЦЕМЕНТ. БЕТОН. СУХИЕ СМЕСИ»
								</h4>
								<span class="lkDownloadTicket__data">
									28-30 ноября 2018 года  |  Москва. Экспоцентр
								</span>
							</div>
						</div>
						<!-- /.lkDownloadTicket__titlebox -->

						<ul class="lkDownloadTicket__list">
							<li class="lkDownloadTicket__item justi_cont">
								<div class="lkDownloadTicket__name">
									Николаев Анатолий
								</div>
								<div class="lkDownloadTicket__downmail">
									<a href="" class="lkDownloadTicket__link lkDownloadTicket__link_marginright">
										Скачать
									</a>
									<a href="" class="lkDownloadTicket__link">
										Отправить по e-mail
									</a>
								</div>
							</li>
							<li class="lkDownloadTicket__item justi_cont">
								<div class="lkDownloadTicket__name">
									Николаев Анатолий
								</div>
								<div class="lkDownloadTicket__downmail">
									<a href="" class="lkDownloadTicket__link lkDownloadTicket__link_marginright">
										Скачать
									</a>
									<a href="" class="lkDownloadTicket__link">
										Отправить по e-mail
									</a>
								</div>
							</li>
							<li class="lkDownloadTicket__item justi_cont">
								<div class="lkDownloadTicket__name">
									Николаев Анатолий
								</div>
								<div class="lkDownloadTicket__downmail">
									<a href="" class="lkDownloadTicket__link lkDownloadTicket__link_marginright">
										Скачать
									</a>
									<a href="" class="lkDownloadTicket__link">
										Отправить по e-mail
									</a>
								</div>
							</li>
							<li class="lkDownloadTicket__item justi_cont">
								<div class="lkDownloadTicket__name">
									Николаев Анатолий
								</div>
								<div class="lkDownloadTicket__downmail">
									<a href="" class="lkDownloadTicket__link lkDownloadTicket__link_marginright">
										Скачать
									</a>
									<a href="" class="lkDownloadTicket__link">
										Отправить по e-mail
									</a>
								</div>
							</li>
						</ul>
						<!-- /.lkDownloadTicket__list -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- /.container_fluid -->
	
</section>

<?php get_footer() ?>