<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'infocem');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(TuyulaidY> +S8b,omHEXi|9|$5W0m{:|!+!6m+{ZxUkk;xTLo47UyHb_PuCv*c');
define('SECURE_AUTH_KEY',  '_{[/(^-6OC$5cCN;_#g$XC9P:<,&Avy,:>:qyG-|PM>/[#c:B?Y*lZ;(SjIrK*_%');
define('LOGGED_IN_KEY',    'H;5TxQ/QK4!78x~MZkAyHbr<R+Q!a!Fo(QvVLE%_)$|omzOQqE&/IHn/*/<e)B%B');
define('NONCE_KEY',        'TY_l0/KSd0oz.5Vu+Gmh0Hdx(odTe$X-1e2xO[I;nsk_FXn=A3;A{CO95B>NdjX|');
define('AUTH_SALT',        '2yjSD2YpBR~E}I@FZO]&tz~Qi%(^du<,bS*8dyiMSvQeBdKj_]2s/U!pK<4;g0S;');
define('SECURE_AUTH_SALT', 'P0+vN@GKr)#H`[yxCU}&PdsqU/d7THJomZP7a)E*!Lup$D.a }knE2:9aAUE9y+E');
define('LOGGED_IN_SALT',   ':XD9`Uju^$>D5(/<c1dsqPpDVK61t ?4yAW!2zC6W^DHOF7Mi5v`baRV~&et5k=*');
define('NONCE_SALT',       '`XKQ~-1*=@n)aZ)n%Ut?>e$[]Bcat_NDnjF0i=,`1gApj;S}/c3WBkfv>xlV29}y');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
